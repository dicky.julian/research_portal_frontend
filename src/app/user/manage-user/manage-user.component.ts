import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccManager, ClientType, Country, Role } from '@core/domain-classes/role';
import { User } from '@core/domain-classes/user';
import { CommonService } from '@core/services/common.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { UserService } from '../user.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css']
})
export class ManageUserComponent extends BaseComponent implements OnInit {

  @ViewChild('newPassword') newPassInput: ElementRef<HTMLInputElement>;
  isNewPassShowed = false;

  @ViewChild('confirmPassword') confirmPassInput: ElementRef<HTMLInputElement>;
  isConfirmPassShowed = false;

  user: User;
  userForm: UntypedFormGroup;
  roleList: Role[] = new Array;
  clientTypes: ClientType[] = new Array;
  accManagers: AccManager[] = new Array;
  countries: Country[] = new Array;
  isEditMode = false;
  selectedRoles: Role[] = [];
  selectedClientType: ClientType[] = new Array;
  selectedAccManager: AccManager[] = new Array;
  selectedCountry: Country[] = new Array;
  isRoleUserValid:boolean = true;
  isClientTypeUserValid:boolean = true;
  isAccManagerUserValid:boolean = true;
  isCountryUserValid:boolean = true;
  isPassContainsAlphanumeric:boolean = true;
  isPassNotContainsSpaceFirstAndLast:boolean = true;
  isPassContainsSpecialChar:boolean = true;
  symbolChar = environment.symbol;

  prefixGeneratedPass:string = "";
  suffixGeneratedPass:string = "";

  constructor(
    private fb: UntypedFormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private userService: UserService,
    private toastrService: ToastrService,
    private commonService: CommonService,
    private translationService:TranslationService
    ) {
    super();
  }

  ngOnInit(): void {
    this.createUserForm();
    this.sub$.sink = this.activeRoute.data.subscribe(
      (data: { user: User }) => {
        if (data.user) {
          this.isEditMode = true;
          this.userForm.patchValue(data.user);
          this.user = data.user;
        } else {
          this.userForm.get('password').setValidators([Validators.required, Validators.minLength(8)]);
          this.userForm.get('confirmPassword').setValidators([Validators.required]);
        }
      });
    this.getRoles();
    this.getClientTypes();
    this.getAccManagers();
    this.getCountries();

    this.prefixGeneratedPass = environment.prefixGeneratedPass;

    const currentDate = new Date;
    this.suffixGeneratedPass = (currentDate.getFullYear()).toString();
  }

  createUserForm() {
    this.userForm = this.fb.group({
      id: [''],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required]],
      companyName: ['', [Validators.required]],
      password: [''],
      confirmPassword: [''],
    }, {
      validator: this.checkPasswords
    });
  }

  checkPasswords(group: UntypedFormGroup) {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : { notSame: true }
  }

  validatePassRequirement(){
    const password = (this.userForm.get('password').value).toString();
    if (/\d/.test(password) && /[a-zA-Z]/.test(password)) {
      this.isPassContainsAlphanumeric = true;
    } else {
      this.isPassContainsAlphanumeric = false;
    }

    if (/^\s+/.test(password) || /\s+$/.test(password)){
      this.isPassNotContainsSpaceFirstAndLast = false;
    } else {
      this.isPassNotContainsSpaceFirstAndLast = true;
    }

    if (/[\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\|\\\:\;\"\'\<\,\>\.\?\/]/.test(password)) {
      this.isPassContainsSpecialChar = true;
    } else {
      this.isPassContainsSpecialChar = false;
    }
  }

  autoGeneratePass(){
    this.userForm.get("password").setValue(this.prefixGeneratedPass+this.userForm.get("firstName").value+this.suffixGeneratedPass);
    this.userForm.get("confirmPassword").setValue(this.prefixGeneratedPass+this.userForm.get("firstName").value+this.suffixGeneratedPass);
    this.validatePassRequirement();
    this.userForm.updateValueAndValidity();
  }

  saveUser() {
    this.isRoleUserValid = this.selectedRoles.length > 0;
    this.isClientTypeUserValid = this.selectedClientType.length > 0;
    this.isAccManagerUserValid = this.selectedAccManager.length > 0;
    this.isCountryUserValid = this.selectedCountry.length > 0;

    const isValid = this.isRoleUserValid 
      && this.isClientTypeUserValid
      && this.isAccManagerUserValid
      && this.isCountryUserValid
      && this.isPassContainsAlphanumeric 
      && this.isPassNotContainsSpaceFirstAndLast 
      && this.isPassContainsSpecialChar;

    if (this.userForm.valid && isValid) {
      const user = this.createBuildObject();
      if (this.isEditMode) {
        this.sub$.sink = this.userService.updateUser(user).subscribe(() => {
          this.toastrService.success(this.translationService.getValue('USER_UPDATED_SUCCESSFULLY'));
          this.router.navigate(['/users']);
        });
      } else {
        this.sub$.sink = this.userService.addUser(user).subscribe(() => {
          this.toastrService.success(this.translationService.getValue('USER_CREATED_SUCCESSFULLY'));
          this.router.navigate(['/users']);
        },(err) => {
          this.toastrService.error(this.translationService.getValue('USER_ALREADY_EXIST'));
        });
      }
    } else {
      this.markFormGroupTouched(this.userForm);
      this.toastrService.error(this.translationService.getValue('PLEASE_ENTER_PROPER_DATA'))
    }
  }

  private markFormGroupTouched(formGroup: UntypedFormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  createBuildObject(): User {
    return {
      id: this.userForm.get('id').value,
      firstName: this.userForm.get('firstName').value,
      lastName: this.userForm.get('lastName').value,
      email: this.userForm.get('email').value,
      phoneNumber: this.userForm.get('phoneNumber').value,
      companyName: this.userForm.get('companyName').value,
      password: this.userForm.get('password').value,
      userName: this.userForm.get('email').value,
      userRoles: this.getSelectedRoles(),
      clientCode: this.getSelectedClientType(),
      idMgr: this.getSelectedAccManager(),
      ctyCode: this.getSelectedCountry()
    }
  }

  showHideNewPass() {
    if (this.newPassInput.nativeElement.type === "password") {
      this.newPassInput.nativeElement.type = "text";
      this.isNewPassShowed = true;
    } else {
      this.newPassInput.nativeElement.type = "password";
      this.isNewPassShowed = false;
    }
  }

  showHideConfirmPass() {
    if (this.confirmPassInput.nativeElement.type === "password") {
      this.confirmPassInput.nativeElement.type = "text";
      this.isConfirmPassShowed = true;
    } else {
      this.confirmPassInput.nativeElement.type = "password";
      this.isConfirmPassShowed = false;
    }
  }

  getSelectedRoles() {
    return this.selectedRoles.map((role) => {
      return {
        userId: this.userForm.get('id').value,
        roleId: role.id
      }
    })
  }

  getSelectedClientType() {
    return this.selectedClientType[0].clientCode;
  }

  getSelectedAccManager() {
    return this.selectedAccManager[0].id_mgr;
  }

  getSelectedCountry() {
    return this.selectedCountry[0].ctyCode;
  }

  getRoles() {
    this.sub$.sink = this.commonService.getRoles().subscribe((roles: Role[]) => {
      // this.roleList = roles;
      let isSuperAdmin:boolean = false;
      roles.forEach(role => {
        if (localStorage.getItem('userRole') == role.id && role.id == "2bfc619d-6e13-48ac-869e-3daa59c65115") {
          isSuperAdmin = true;
        }
      });

      roles.forEach(role => {
        if (!isSuperAdmin) {
          if (role.id != "3ad91c85-f648-4abd-adac-426779ccf06b" && role.id != "2bfc619d-6e13-48ac-869e-3daa59c65115") {
            this.roleList.push(role);
          }
        } else{
          this.roleList.push(role);
        }
      });

      if (this.isEditMode) {
        const selectedRoleIds = this.user.userRoles.map(c => c.roleId);
        this.selectedRoles = this.roleList.filter(c => selectedRoleIds.indexOf(c.id) > -1);
      }
    });
  }

  getClientTypes() {
    this.sub$.sink = this.commonService.getClientTypes().subscribe((clientTypes: ClientType[]) => {
      this.clientTypes = clientTypes;

      if (this.isEditMode) {
        this.selectedClientType = clientTypes.filter(c => c.clientCode === this.user.clientCode);
      }
    });
  }

  getAccManagers() {
    this.sub$.sink = this.commonService.getAccManagers().subscribe((accManagers: AccManager[]) => {
      this.accManagers = accManagers;

      if (this.isEditMode) {
        this.selectedAccManager = accManagers.filter(c => c.id_mgr === this.user.idMgr);
      }
    });
  }

  getCountries() {
    this.sub$.sink = this.commonService.getCountries().subscribe((countries: Country[]) => {
      this.countries = countries;

      if (this.isEditMode) {
        this.selectedCountry = countries.filter(c => c.ctyCode === this.user.ctyCode);
      }
    });
  }
}
