import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '@core/domain-classes/user';
import { SecurityService } from '@core/security/security.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { UserService } from '../user.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})

export class ChangePasswordComponent extends BaseComponent implements OnInit {
  @ViewChild('newPassword') newPassInput: ElementRef<HTMLInputElement>;
  isNewPassShowed = false;

  @ViewChild('confirmPassword') confirmPassInput: ElementRef<HTMLInputElement>;
  isConfirmPassShowed = false;

  changePasswordForm: UntypedFormGroup;
  isResetPassword:string = '';
  isPassContainsAlphanumeric:boolean = true;
  isPassNotContainsSpaceFirstAndLast:boolean = true;
  isPassContainsSpecialChar:boolean = true;
  symbolChar = environment.symbol;

  constructor(
    private userService: UserService,
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private toastrService: ToastrService,
    private securityService: SecurityService,
    private translationService:TranslationService) {
    super();
  }

  ngOnInit(): void {
    this.createChangePasswordForm();
    this.changePasswordForm.get('email').setValue(this.data.userName);
    this.isResetPassword = localStorage.getItem('isResetPassword');
  }

  createChangePasswordForm() {
    this.changePasswordForm = this.fb.group({
      email: [],
      oldPasswordPassword: [''],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: this.checkPasswords
    });
  }

  checkPasswords(group: UntypedFormGroup) {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : { notSame: true }
  }

  validatePassRequirement(){
    const password = (this.changePasswordForm.get('password').value).toString();
    if (/\d/.test(password) && /[a-zA-Z]/.test(password)) {
      this.isPassContainsAlphanumeric = true;
    } else {
      this.isPassContainsAlphanumeric = false;
    }

    if(/^\s+/.test(password) || /\s+$/.test(password)){
      this.isPassNotContainsSpaceFirstAndLast = false;
    } else {
      this.isPassNotContainsSpaceFirstAndLast = true;
    }

    if (/[\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\|\\\:\;\"\'\<\,\>\.\?\/]/.test(password)) {
      this.isPassContainsSpecialChar = true;
    } else {
      this.isPassContainsSpecialChar = false;
    }
  }

  changePassword() {
    if (this.changePasswordForm.valid && this.isPassContainsAlphanumeric && this.isPassNotContainsSpaceFirstAndLast && this.isPassContainsSpecialChar) {
      this.sub$.sink = this.userService.changePassword2(this.createBuildObject()).subscribe(d => {
        this.toastrService.success(this.translationService.getValue('SUCCESSFULLY_CHANGED_PASSWORD'))
        this.securityService.logout();
        this.dialogRef.close();
      })
    } else {
      this.toastrService.error(this.translationService.getValue('PLEASE_ENTER_PROPER_DATA'));
    }
  }

  createBuildObject() {
    return {
      email: this.changePasswordForm.get('email').value,
      oldPassword: this.changePasswordForm.get('oldPasswordPassword').value,
      newPassword: this.changePasswordForm.get('password').value,
      userName: this.changePasswordForm.get('email').value,
    }
  }

  showHideNewPass() {
    if (this.newPassInput.nativeElement.type === "password") {
      this.newPassInput.nativeElement.type = "text";
      this.isNewPassShowed = true;
    } else {
      this.newPassInput.nativeElement.type = "password";
      this.isNewPassShowed = false;
    }
  }

  showHideConfirmPass() {
    if (this.confirmPassInput.nativeElement.type === "password") {
      this.confirmPassInput.nativeElement.type = "text";
      this.isConfirmPassShowed = true;
    } else {
      this.confirmPassInput.nativeElement.type = "password";
      this.isConfirmPassShowed = false;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
