import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '@core/domain-classes/user';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { UserService } from '../user.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent extends BaseComponent implements OnInit {
  @ViewChild('newPassword') newPassInput: ElementRef<HTMLInputElement>;
  isNewPassShowed = false;

  @ViewChild('confirmPassword') confirmPassInput: ElementRef<HTMLInputElement>;
  isConfirmPassShowed = false;

  isResetPassword:string = '';
  isPassContainsAlphanumeric:boolean = true;
  isPassNotContainsSpaceFirstAndLast:boolean = true;
  isPassContainsSpecialChar:boolean = true;
  symbolChar = environment.symbol;

  resetPasswordForm: UntypedFormGroup;

  constructor(
    private userService: UserService,
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<ResetPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private toastrService: ToastrService,
    private translationService:TranslationService) {
    super();
  }

  ngOnInit(): void {
    this.createResetPasswordForm();
    this.resetPasswordForm.get('email').setValue(this.data.userName);
  }

  createResetPasswordForm() {
    this.resetPasswordForm = this.fb.group({
      email: [],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: this.checkPasswords
    });
  }

  checkPasswords(group: UntypedFormGroup) {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : { notSame: true }
  }

  resetPassword() {
    if (this.resetPasswordForm.valid && this.isPassContainsAlphanumeric && this.isPassNotContainsSpaceFirstAndLast && this.isPassContainsSpecialChar) {
      this.sub$.sink = this.userService.resetPassword(this.createBuildObject()).subscribe(d => {
        this.toastrService.success(this.translationService.getValue('SUCCESSFULLY_RESET_PASSWORD'))
        this.dialogRef.close();
      })
    } else {
      this.toastrService.error(this.translationService.getValue('PLEASE_ENTER_PROPER_DATA'));
    }
  }

  createBuildObject(): User {
    return {
      email: '',
      password: this.resetPasswordForm.get('password').value,
      userName: this.resetPasswordForm.get('email').value,
    }
  }

  validatePassRequirement(){
    const password = (this.resetPasswordForm.get('password').value).toString();
    if (/\d/.test(password) && /[a-zA-Z]/.test(password)) {
      this.isPassContainsAlphanumeric = true;
    } else {
      this.isPassContainsAlphanumeric = false;
    }

    if(/^\s+/.test(password) || /\s+$/.test(password)){
      this.isPassNotContainsSpaceFirstAndLast = false;
    } else {
      this.isPassNotContainsSpaceFirstAndLast = true;
    }

    if (/[\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\|\\\:\;\"\'\<\,\>\.\?\/]/.test(password)) {
      this.isPassContainsSpecialChar = true;
    } else {
      this.isPassContainsSpecialChar = false;
    }
  }

  showHideNewPass() {
    if (this.newPassInput.nativeElement.type === "password") {
      this.newPassInput.nativeElement.type = "text";
      this.isNewPassShowed = true;
    } else {
      this.newPassInput.nativeElement.type = "password";
      this.isNewPassShowed = false;
    }
  }

  showHideConfirmPass() {
    if (this.confirmPassInput.nativeElement.type === "password") {
      this.confirmPassInput.nativeElement.type = "text";
      this.isConfirmPassShowed = true;
    } else {
      this.confirmPassInput.nativeElement.type = "password";
      this.isConfirmPassShowed = false;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
