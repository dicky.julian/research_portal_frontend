import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DocumentByCategory } from '@core/domain-classes/document-by-category';
import { catchError } from 'rxjs/operators';
import { CommonError } from '@core/error-handler/common-error';
import { CommonHttpErrorService } from '@core/error-handler/common-http-error.service';
import { CalenderReminderDto } from '@core/domain-classes/calender-reminder';
import { DocumentResource } from '@core/domain-classes/document-resource';
import { DocumentInfo } from '@core/domain-classes/document-info';

@Injectable({ providedIn: 'root' })
export class DashboradService {
  constructor(private httpClient: HttpClient,
    private commonHttpErrorService: CommonHttpErrorService) { }

  getDocumentByCategory(): Observable<DocumentByCategory[]> {
    const url = `Dashboard/GetDocumentByCategory`;
    return this.httpClient.get<DocumentByCategory[]>(url);
  }

  getDailyReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/dailyreminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getWeeklyReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/weeklyreminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getMonthlyReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/monthlyreminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getQuarterlyReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/quarterlyreminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getHalfYearlyReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/halfyearlyreminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getYearlyReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/yearlyreminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getOneTimeReminders(month, year): Observable<CalenderReminderDto[] | CommonError> {
    const url = `dashboard/onetimereminder/${month}/${year}`;
    return this.httpClient.get<CalenderReminderDto[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  // ENV USE URL API 2
  getDocumentsForHome(): Observable<HttpResponse<DocumentInfo[]> | CommonError> {
    const url = `__apiUrl2__/research/document`;
    const customParams = new HttpParams()
      .set('ParentId', "")
      .set('RoleId', localStorage.getItem('userRole'))
      .set('UserId', JSON.parse(localStorage.getItem('authObj')).id)
      .set('Size', "5")
      .set('Page', "1")
      .set('Query', "")
      .set('Sort', "desc")
      .set('Analyst', "")
      .set('StartDate', "")
      .set('EndDate', "")

    return this.httpClient.get<DocumentInfo[]>(url, {
      params: customParams,
      observe: 'response'
    }).pipe(catchError(this.commonHttpErrorService.handleError));
  }

}
