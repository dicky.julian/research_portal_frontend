import { Component, OnInit } from '@angular/core';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentView } from '@core/domain-classes/document-view';
import { BasePreviewComponent } from '@shared/base-preview/base-preview.component';
import { OverlayPanel } from '@shared/overlay-panel/overlay-panel.service';
import { DashboradService } from '../dashboard.service';

@Component({
  selector: 'app-top-document-list',
  templateUrl: './top-document-list.component.html',
  styleUrls: ['./top-document-list.component.scss']
})
export class TopDocumentListComponent implements OnInit {

  listDoc:Array<any> = new Array();

  constructor(
    private dashboardService: DashboradService,
    public overlay: OverlayPanel,
  ) { }

  ngOnInit(): void {
    this.getListDocForHome();
  }

  getListDocForHome(){
    this.dashboardService.getDocumentsForHome().subscribe(
      (c:any) => {
        this.listDoc = c.body;
      },
      () => {

      }
    );

  }

  onDocumentView(document: DocumentInfo) {
    const urls = document.url.split('.');
    const extension = urls[1];
    const documentView: DocumentView = {
      documentId: document.id,
      name: document.name,
      extension: extension,
      isRestricted: document.isAllowDownload,
      isVersion: false,
      isFromPreview: true
    };
    this.overlay.open(BasePreviewComponent, {
      position: 'center',
      origin: 'global',
      panelClass: ['file-preview-overlay-container', 'white-background'],
      data: documentView
    });
  }

}
