import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopDocumentListComponent } from './top-document-list.component';

describe('TopDocumentListComponent', () => {
  let component: TopDocumentListComponent;
  let fixture: ComponentFixture<TopDocumentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopDocumentListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TopDocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
