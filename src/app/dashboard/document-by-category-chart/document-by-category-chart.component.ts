import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DashboradService } from '../dashboard.service';

@Component({
  selector: 'app-document-by-category-chart',
  templateUrl: './document-by-category-chart.component.html',
  styleUrls: ['./document-by-category-chart.component.scss']
})
export class DocumentByCategoryChartComponent implements OnInit, AfterViewInit {
  single: any[] = [];
  viewPie: any[] = [350, 350];
  viewCard: any[] = [1000, 200];

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  cardColor: string = '#232837';


  constructor(
    private dashboardService: DashboradService
    ) {
      // Object.assign(this, single);
     }

  ngOnInit(): void {

    // document.querySelector('#message').getAttribute.
    this.getDocumentCategoryChartData();
  }

  ngAfterViewInit():void{
    setTimeout(() => {
      if (document.getElementsByClassName('card-wrapper')[0]) {
        let cardWrapperWidth = document.getElementsByClassName('card-wrapper')[0].clientWidth;
        this.viewCard = [cardWrapperWidth, 200];
      }
    });
  }

  getDocumentCategoryChartData() {
    this.dashboardService.getDocumentByCategory().subscribe(data => {
      let tempSingle:any[] = [];
      data.map(c => {
        if (c.documentCount != 0) {
          tempSingle.push({
            name: c.categoryName,
            value: c.documentCount
          })
        }
      })
      this.single = tempSingle;
      // this.single = data.map(c => {
      //     return {
      //       name: c.categoryName,
      //       value: c.documentCount
      //     }
      // });
    });
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
