import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalystComponent } from './analyst.component';
import { AuthGuard } from '@core/security/auth.guard';
import { ManageAnalystComponent } from './manage-analyst/manage-analyst.component';

const routes: Routes = [
  {
    path:'',
    component: AnalystComponent,
    data: { claimType: 'all_documents_view_documents' },
    canActivate: [AuthGuard]
  },
  {
    path:'manage/add',
    component: ManageAnalystComponent,
    data: { claimType: 'role_create_role' },
    canActivate: [AuthGuard]
  },
  {
    path: 'manage/:id',
    component: ManageAnalystComponent,
    data: { claimType: 'role_edit_role' },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalystRoutingModule { }
