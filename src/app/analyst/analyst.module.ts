import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalystComponent, DialogDeleteAnalystComponent } from './analyst.component';
import { AnalystRoutingModule } from './analyst-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { SharedModule } from '@shared/shared.module';
import { ManageAnalystComponent } from './manage-analyst/manage-analyst.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatIconModule} from '@angular/material/icon'

@NgModule({
  declarations: [
    AnalystComponent,
    ManageAnalystComponent,
    DialogDeleteAnalystComponent
  ],
  imports: [
    CommonModule,
    AnalystRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    SharedModule,
    MatProgressBarModule,
    MatIconModule
  ]
})
export class AnalystModule { }
