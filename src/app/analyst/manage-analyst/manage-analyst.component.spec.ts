import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAnalystComponent } from './manage-analyst.component';

describe('ManageAnalystComponent', () => {
  let component: ManageAnalystComponent;
  let fixture: ComponentFixture<ManageAnalystComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageAnalystComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManageAnalystComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
