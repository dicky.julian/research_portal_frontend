import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Analyst } from '@core/domain-classes/analyst';
import { FileInfo } from '@core/domain-classes/file-info';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Observable, ReplaySubject } from 'rxjs';
import { AnalystService } from '../analyst.service';

@Component({
  selector: 'app-manage-analyst',
  templateUrl: './manage-analyst.component.html',
  styleUrls: ['./manage-analyst.component.scss']
})
export class ManageAnalystComponent implements OnInit {
  analyst:Analyst;
  isEditMode = false;
  analystForm: UntypedFormGroup;
  extension: string = '';
  showProgress: boolean = false;
  progress: number = 0;
  isFileUpload: boolean = false;
  fileInfo: FileInfo;
  base64Photo:string = "";
  isFileValid:boolean = false;
  analystId:string= "";

  constructor(
    private activeRoute:ActivatedRoute,
    private fb: UntypedFormBuilder,
    private cd: ChangeDetectorRef,
    private analystService: AnalystService,
    private router:Router,
    private toastService:ToastrService
  ) { }

  ngOnInit(): void {
    this.createUserForm();
    this.activeRoute.params.subscribe(
      params => {
        if (params.id) {
          this.isEditMode = true;
          this.analystId = params.id;
          this.analystService.getAnalystById(params.id).subscribe(
            (s)=>{
              this.analystForm.patchValue(s[0]);
              this.base64Photo = s[0].bytePhoto;

              this.fileUploadValidation('old_photo_valid');
              this.fileUploadExtensionValidation('valid');

              this.analystForm.updateValueAndValidity();
            }
          )
        }
      }
    );
  }

  createUserForm() {
    this.analystForm = this.fb.group({
      name: ['', [Validators.required]],
      title: [''],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      coverage: [''],
      fileName: ['', [Validators.required]],
      extension: ['', [Validators.required]],
    });
  }

  createBuildObject(): Analyst {
    const datas: Analyst = {
      name: this.analystForm.get('name').value,
      title: this.analystForm.get('title').value,
      email: this.analystForm.get('email').value,
      phone: this.analystForm.get('phone').value,
      coverage: this.analystForm.get('coverage').value
    }
    return datas;
  }

  upload(event, files) {
    if (files.length === 0)
      return;
    this.extension = files[0].name.split('.').pop();
    // this.showProgress = true;
    if (!this.fileExtesionValidation(this.extension)) {
      this.fileUploadExtensionValidation('');
      this.showProgress = false;
      this.cd.markForCheck();
      return;
    } else {
      this.fileUploadExtensionValidation('valid');
    }
    // const size = files[0].size;

    // this.fileUploadSizeValidation('valid')

    this.convertImageToBase64(event.target.files[0]).subscribe(base64 => {
      this.base64Photo = base64;
      this.fileUploadValidation('new_photo_valid');
    });
  }

  convertImageToBase64(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event) => result.next(btoa(event.target.result.toString()));
    return result;
  }

  fileUploadExtensionValidation(extension: string) {
    this.analystForm.patchValue({
      extension: extension
    })
    this.analystForm.get('extension').markAsTouched();
    this.analystForm.updateValueAndValidity();
  }

  fileExtesionValidation(extesion: string): boolean {
    const allowExtesions = environment.allowPhotoExtesions;
    var allowTypeExtenstion = allowExtesions.find(c => c.extentions.find(ext => ext === extesion));
    return allowTypeExtenstion ? true : false;
  }

  fileUploadSizeValidation(fileSize: string) {
    this.analystForm.patchValue({
      fileSize: fileSize
    })
    this.analystForm.get('fileSize').markAsTouched();
    this.analystForm.updateValueAndValidity();
  }

  fileUploadValidation(fileName: string) {
    this.analystForm.patchValue({
      fileName: fileName
    })
    this.analystForm.get('fileName').markAsTouched();
    this.analystForm.updateValueAndValidity();
  }

  base64_url_decode(base64:any){
    // base64.replaceAll('.','+').replaceAll('_','/').replaceAll('-','=');
    // base64.replaceAll('_','/');
    // base64.replaceAll('-','=');
    // console.log(base64);
    return base64.replaceAll('_','/').replaceAll('-','+');
  }

  saveAnalyst(){
    if (this.analystForm.valid) {
      const analyst = this.createBuildObject();
      // if (this.analystForm.get('fileName').value == "old_photo_valid") {
      //   analyst.photo = this.base64_url_decode(this.base64Photo);
      // } else{
      //   analyst.photo = this.base64Photo;
      // }
      analyst.photo = this.base64Photo;

      if (this.isEditMode && this.analystId != "") {
        analyst.id = this.analystId;
        this.analystService.updateAnalyst(analyst).subscribe(
          (c) => {
            this.router.navigate(['analysts']);
            this.toastService.success('Success to update analyst');
          },
          (e)=>{
            this.toastService.error('Failed to update analyst');
          }
        )
      } else {
        this.analystService.addAnalyst(analyst).subscribe(
          (c) => {
            this.router.navigate(['analysts']);
            this.toastService.success('Success to add analyst');
          },
          (e)=>{
            this.toastService.error('Failed to add analyst');
          }
        )
      }
    }
  }
}
