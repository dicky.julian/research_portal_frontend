import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Analyst } from '@core/domain-classes/analyst';
import { ToastrService } from 'ngx-toastr';
import { AnalystService } from './analyst.service';

@Component({
  selector: 'app-analyst',
  templateUrl: './analyst.component.html',
  styleUrls: ['./analyst.component.scss']
})
export class AnalystComponent implements OnInit {

  analystDatas:Array<Analyst>;
  analystsLoading:boolean =true;

  constructor(
    private analystService: AnalystService,
    private toastrService: ToastrService,
    public router:Router,
    private dialog:MatDialog
  ) { }

  ngOnInit(): void {
    this.getAnalysts();
  }

  getAnalysts(){
    this.analystService.getAllAnalyst().subscribe(
      (s) => {
        this.analystDatas = [];
        this.analystDatas = s;
      },
      (err) =>{
        this.toastrService.error("Error load data");
        this.analystsLoading = true;
      },
      () => {
        this.analystsLoading = false;
      }
    );
  }

  base64_url_decode(base64:any){
    // base64.replaceAll('.','+').replaceAll('_','/').replaceAll('-','=');
    // base64.replaceAll('_','/');
    // base64.replaceAll('-','=');
    // console.log(base64);
    return base64.replaceAll('_','/').replaceAll('-','+');
  }

  editAnalyst(analystId: string){
    this.router.navigate(['/analysts/manage',analystId])
  }

  deleteAnalyst(analyst:Analyst){
    const dialogRef = this.dialog.open(DialogDeleteAnalystComponent, {
      width: '300px',
      panelClass: 'full-width-dialog',
      disableClose: false,
      data:{analyst}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAnalysts();
    });
  }
}

@Component({
  selector: 'dialog-delete-analyst',
  templateUrl: './dialog-delete-analyst.component.html',
})
export class DialogDeleteAnalystComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogDeleteAnalystComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private analystService: AnalystService,
    private toastrService: ToastrService,
    ) {}

    deleteAnalyst(analystId){
      this.analystService.deleteAnalyst(analystId).subscribe(
          (s)=> {
            this.toastrService.success("Delete Analyst Success");
            this.dialogRef.close();
          },
          (e) => {
            this.toastrService.error("Delete Analyst Error");
            this.dialogRef.close();
          }
        )
    }
}
