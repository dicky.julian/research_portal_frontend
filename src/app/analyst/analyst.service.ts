import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Analyst } from '@core/domain-classes/analyst';
import { CommonHttpErrorService } from '@core/error-handler/common-http-error.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnalystService {

  constructor(
    private httpClient: HttpClient,
    private commonHttpErrorService: CommonHttpErrorService,
    private toastrService: ToastrService,
    private translationService: TranslationService
  ) { }

  getAllAnalyst(): Observable<any[]> {
    const url = `__apiUrl2__/research/analysts/all/1`;
    return this.httpClient.get<any[]>(url);
  }

  getAnalystById(id:string): Observable<any[]> {
    const url = `__apiUrl2__/research/analysts/${id}/1`;
    return this.httpClient.get<any[]>(url);
  }

  addAnalyst(data:Analyst): Observable<any[]> {
    const url = `__apiUrl2__/research/analysts`;
    return this.httpClient.post<any[]>(url, data);
  }

  updateAnalyst(data:Analyst): Observable<any[]> {
    const url = `__apiUrl2__/research/analysts/${data.id}`;
    return this.httpClient.put<any>(url, data);
  }

  deleteAnalyst(id:string): Observable<any[]> {
    const url = `__apiUrl2__/research/analysts/${id}`;
    return this.httpClient.delete<any[]>(url);
  }
}
