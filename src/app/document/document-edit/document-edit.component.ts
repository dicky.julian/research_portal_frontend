import { Component, Inject, Input, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from '@core/domain-classes/category';
import { DocumentAuditTrail } from '@core/domain-classes/document-audit-trail';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentOperation } from '@core/domain-classes/document-operation';
import { DocumentMetaData } from '@core/domain-classes/documentMetaData';
import { CommonService } from '@core/services/common.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { DocumentService } from '../document.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CategoryService } from '@core/services/category.service';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-document-edit',
  templateUrl: './document-edit.component.html',
  styleUrls: ['./document-edit.component.scss']
})
export class DocumentEditComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  document: DocumentInfo;
  documentForm: UntypedFormGroup;
  extension: string = '';
  @Input() categories: Category[];
  @Input() allAnalyst: any[];
  // @Input() currentCatUrlActive: any;
  allCategories: Category[] = [];
  @Input() documentInfo: DocumentInfo;
  documentSource: string;
  isAnalystValid:boolean = true;
  tempCatId:string[] = [];

  isSearchCat:boolean = false;
  public bankCtrl: FormControl<Category> = new FormControl<Category>(null);
  public bankFilterCtrl: FormControl<string> = new FormControl<string>('');
  public filteredBanks: ReplaySubject<Category[]> = new ReplaySubject<Category[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  protected _onDestroy = new Subject<void>();

  get documentMetaTagsArray(): FormArray {
    return <FormArray>this.documentForm.get('documentMetaTags');
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
        'fontName'
      ],
      [
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
        'backgroundColor'
      ]
    ]
  };

  constructor(
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<DocumentEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastrService: ToastrService,
    private documentService: DocumentService,
    private commonService: CommonService,
    private translationService: TranslationService,
    private categoryService:CategoryService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createDocumentForm();
    this.pushValuesDocumentMetatagArray();
    this.patchDocumentForm();
    this.getCategories();

    this.filteredBanks.next(this.allCategories);

    this.bankFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredBanks
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // this.singleSelect.compareWith = (a: Category, b: Category) => a && b && a.id === b.id;
      });
  }

  protected filterBanks() {
    if (!this.allCategories) {
      return;
    }

    let search = this.bankFilterCtrl.value;
    if (!search) {
      this.filteredBanks.next(this.allCategories.slice());
      this.isSearchCat = false;
      return;
    } else {
      search = search.toLowerCase();
      this.isSearchCat = true;
    }

    this.filteredBanks.next(
      this.allCategories.filter(cat => cat.name.toLowerCase().indexOf(search) > -1)
    );
  }

  getCategories() {
    this.categoryService.getAllCategoriesForDropDown().subscribe(c => {
      this.categories = c;
      this.tempCatId = [localStorage.getItem('idCatReport')];
      this.setDeafLevel();
    });
  }

  setDeafLevel(parent?: Category, parentId?: string) {
    const children = this.categories.filter(c => c.parentId == parentId);
    if (children.length > 0) {
      children.map((c, index) => {
        c.deafLevel = parent ? parent.deafLevel + 1 : 0;
        c.index = (parent ? parent.index : 0) + index * Math.pow(0.1, c.deafLevel);
        if (localStorage.getItem('idCatReport')) {
          if (c.parentId != null) {
            this.tempCatId.push(c.id);
          }
          this.tempCatId.map((val, idx) =>{
            if (val == c.id || val == c.parentId) {
              this.allCategories.push(c);
              this.tempCatId.splice(idx, 1);
              this.setDeafLevel(c, c.id);
            }
            // else if () {
            //   this.allCategories.push(c);
            //   this.tempCatId.splice(idx, 1);
            //   this.setDeafLevel(c, c.id);
            // }
          })
        } else {
          this.allCategories.push(c);
          this.setDeafLevel(c, c.id);
        }
      });
    }
    return parent;
  }

  // getCategories() {
  //   this.setDeafLevel();
  // }

  // setDeafLevel(parent?: Category, parentId?: string) {
  //   const children = this.data.categories.filter(c => c.parentId == parentId);
  //   if (children.length > 0) {
  //     children.map((c, index) => {
  //       c.deafLevel = parent ? parent.deafLevel + 1 : 0;
  //       c.index = (parent ? parent.index : 0) + index * Math.pow(0.1, c.deafLevel);
  //       this.allCategories.push(c);
  //       this.setDeafLevel(c, c.id);
  //     });
  //   }
  //   return parent;
  // }

  patchDocumentForm() {
    this.documentForm.patchValue({
      name: this.data.document.name,
      description: this.data.document.description,
      categoryId: this.data.document.categoryId,
      documentMetaDatas: this.data.documentMetaDatas,
      createddate: this.data.document.createdDate,
    })
  }

  createDocumentForm() {
    this.documentForm = this.fb.group({
      name: ['', [Validators.required]],
      description: [''],
      categoryId: ['', [Validators.required]],
      documentMetaTags: this.fb.array([]),
      createddate: ['', [Validators.required]],
    });
  }

  SaveDocument() {
    this.isAnalystValid = true;
    this.documentMetaTagsArray.value.forEach((element) => {
      if (element.metatag == '') {
        this.isAnalystValid = false;
        return;
      } else{
        if (this.documentMetaTagsArray.value.filter(c => c.metatag == element.metatag).length > 1) {
          this.isAnalystValid = false;
          return;
        }
      }
    });

    if (this.documentForm.valid && this.isAnalystValid) {
      this.sub$.sink = this.documentService.updateDocument(this.buildDocumentObject()).subscribe(
        (c:DocumentInfo) => {
          const tempReq = new XMLHttpRequest();
          tempReq.open('GET', "", false);
          tempReq.send(null);

          const currentDate = new Date(tempReq.getResponseHeader("Date"));
          let tempCreatedDate = new Date(this.documentForm.get('createddate').value);
          tempCreatedDate.setHours(currentDate.getHours());
          tempCreatedDate.setMinutes(currentDate.getMinutes());
          tempCreatedDate.setSeconds(currentDate.getSeconds());

          let mm = (tempCreatedDate.getMonth()+1).toString();
          let dd = tempCreatedDate.getDate().toString();
          let hh = tempCreatedDate.getHours().toString();
          let minutes = tempCreatedDate.getMinutes().toString();
          let ss = tempCreatedDate.getSeconds().toString();

          if ((tempCreatedDate.getMonth()+1) < 10) mm = '0' + mm;
          if (tempCreatedDate.getDate() < 10) dd = '0' + dd;
          if (tempCreatedDate.getHours() < 10) hh = '0' + hh;
          if (tempCreatedDate.getMinutes() < 10) minutes = '0' + minutes;
          if (tempCreatedDate.getSeconds() < 10) ss = '0' + ss;

          const createdDate = tempCreatedDate.getFullYear() + "-" + mm + "-" + dd  + 'T' + hh + ':' + minutes + ':' + ss + "Z";

          this.documentService.updateDocumentDate(c.id, createdDate).subscribe(
            d => {
              this.toastrService.success(this.translationService.getValue('DOCUMENT_UPDATE_SUCCESSFULLY'));
              this.dialogRef.close("loaded");
              this.addDocumentTrail();
            }
          )
        },
        (err) => {
          this.toastrService.error(err.error.messages);
        }
        );
    } else {
      this.markFormGroupTouched(this.documentForm);
    }
  }

  addDocumentTrail() {
    const objDocumentAuditTrail: DocumentAuditTrail = {
      documentId: this.data.document.id,
      operationName: DocumentOperation.Modified.toString()
    }
    this.sub$.sink = this.commonService.addDocumentAuditTrail(objDocumentAuditTrail)
      .subscribe(c => {
      })
  }

  private markFormGroupTouched(formGroup: UntypedFormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  buildDocumentObject(): DocumentInfo {
    const documentMetaTags = this.documentMetaTagsArray.value;
    const document: DocumentInfo = {
      id: this.data.document.id,
      categoryId: this.documentForm.get('categoryId').value,
      description: (this.documentForm.get('description').value).trim(),
      name: (this.documentForm.get('name').value).trim(),
      documentMetaDatas: [...documentMetaTags],
    };
    return document;
  }
  onDocumentCancel() {
    this.dialogRef.close("canceled");
  }

  onAddAnotherMetaTag() {
    const documentMetaTag: DocumentMetaData = {
      id: '',
      documentId: this.document && this.document.id ? this.document.id : '',
      metatag: ''
    }
    this.documentMetaTagsArray.insert(0, this.editDocmentMetaData(documentMetaTag));
  }

  onDeleteMetaTag(index: number) {
    this.documentMetaTagsArray.removeAt(index);
  }

  buildDocumentMetaTag(): FormGroup {
    return this.fb.group({
      id: [''],
      documentId: [''],
      metatag: ['']
    });
  }

  pushValuesDocumentMetatagArray() {
    this.sub$.sink = this.documentService.getdocumentMetadataById(this.data.document.id)
      .subscribe((result: DocumentMetaData[]) => {
        if (result.length > 0) {
          result.map(documentMetatag => {
            this.documentMetaTagsArray.push(this.editDocmentMetaData(documentMetatag));
          })
        }
        else {
          this.documentMetaTagsArray.push(this.buildDocumentMetaTag());
        }
      });
  }

  onMetatagChange(event: any, index: number) {
    const analystName = this.documentMetaTagsArray.at(index).get('metatag').value;
    if (!analystName) {
      return;
    }
    const analystNameControl = this.documentMetaTagsArray.at(index).get('metatag');
    analystNameControl.setValidators([Validators.required]);
    analystNameControl.updateValueAndValidity();

  }

  editDocmentMetaData(documentMetatag: DocumentMetaData): FormGroup {
    return this.fb.group({
      id: [documentMetatag.id],
      documentId: [documentMetatag.documentId],
      metatag: [documentMetatag.metatag]
    });
  }

}
