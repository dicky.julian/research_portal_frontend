import { SelectionModel } from '@angular/cdk/collections';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonDialogService } from '@core/common-dialog/common-dialog.service';
import { Category } from '@core/domain-classes/category';
import { DocumentAuditTrail } from '@core/domain-classes/document-audit-trail';
import { ResponseHeader } from '@core/domain-classes/document-header';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentOperation } from '@core/domain-classes/document-operation';
import { DocumentResource } from '@core/domain-classes/document-resource';
import { DocumentView } from '@core/domain-classes/document-view';
import { DocumentVersion } from '@core/domain-classes/documentVersion';
import { CategoryService } from '@core/services/category.service';
import { ClonerService } from '@core/services/clone.service';
import { CommonService } from '@core/services/common.service';
import { TranslationService } from '@core/services/translation.service';
import { BasePreviewComponent } from '@shared/base-preview/base-preview.component';
import { DataSharedService } from '@shared/data-shared/data-shared.service';
import { OverlayPanel } from '@shared/overlay-panel/overlay-panel.service';
import { ToastrService } from 'ngx-toastr';
import { fromEvent, merge, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { AnalystService } from 'src/app/analyst/analyst.service';
import { BaseComponent } from 'src/app/base.component';
import { RoleService } from 'src/app/role/role.service';
import { DocumentEditComponent } from '../document-edit/document-edit.component';
import { DocumentGetLinkComponent } from '../document-get-link/document-get-link.component';
import { DocumentPermissionListComponent } from '../document-permission/document-permission-list/document-permission-list.component';
import { DocumentPermissionMultipleComponent } from '../document-permission/document-permission-multiple/document-permission-multiple.component';
import { DocumentUploadNewVersionComponent } from '../document-upload-new-version/document-upload-new-version.component';
import { DocumentVersionHistoryComponent } from '../document-version-history/document-version-history.component';
import { DocumentService } from '../document.service';
import { DocumentDataSource } from './document-datasource';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent extends BaseComponent implements OnInit, AfterViewInit {
  dataSource: DocumentDataSource;
  documents: DocumentInfo[] = [];
  displayedColumns: string[] = ['action', 'name', 'description', 'categoryName', 'analystName', 'createdDate'];
  displayedColumns2: string[] = ['datas'];
  isLoadingResults = true;
  documentResource: DocumentResource;
  categories: Category[] = [];
  allCategories: Category[] = [];
  allAnalyst: any[] = [];
  loading$: Observable<boolean>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;
  @ViewChild('metatag') metatag: ElementRef;
  selection = new SelectionModel<DocumentInfo>(true, []);
  tempCatId:string[] = [];
  currentCatName:string = "Document";
  currentCatId:string = "";
  currentCatUrlActive:string = "";
  isUserClient:boolean = false;


  sectorVal:string = "";
  subSectorVal:string = "";
  companyVal:string = "";
  selectedAnalyst:string ="";

  listSectors:Array<any> = new Array();
  listSubSectors:Array<any> = new Array();
  listCompanies:Array<any> = new Array();

  rangeDatePicker = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

  constructor(
    private documentService: DocumentService,
    private commonDialogService: CommonDialogService,
    private categoryService: CategoryService,
    private dialog: MatDialog,
    public overlay: OverlayPanel,
    public clonerService: ClonerService,
    private translationService: TranslationService,
    private commonService: CommonService,
    private toastrService: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private roleService: RoleService,
    private sharedDataService: DataSharedService,
    private analystService: AnalystService
  ) {
    super();
    this.documentResource = new DocumentResource();
    this.documentResource.pageSize = 5;
    this.documentResource.orderBy = "CreatedDate desc";
    this.documentResource.startDate = "";
    this.documentResource.endDate = "";

  }

  ngOnInit(): void {
    this.roleService.getRole(localStorage.getItem("userRole")).subscribe(
      (roleResp:any) => {
        if (roleResp.id == 'fa04647a-0a36-4472-94ae-3826f2421237') {
          this.isUserClient = true;
        }
      }
    );

    if (this.route.snapshot.params.cat) {
      this.route.params.subscribe(
        params => {
          this.currentCatUrlActive = params.cat;
          this.dataSource = new DocumentDataSource(this.documentService);
          this.getCategories();
          this.getAllAnalyst();

          this.rangeDatePicker.setValue({start: null, end: null});
          this.sectorVal = "";
          this.subSectorVal = "";
          this.companyVal = "";
          this.selectedAnalyst = "";

          this.sharedDataService.set_searchData("");

        }
      );
    } else {
      this.router.navigate(['/document/company-report']);
    }

    this.sharedDataService.datas.subscribe(
      (datas) => {
        if (this.paginator && this.documentResource && (this.documentResource.name != datas.searchData)) {
          this.paginator.pageIndex = 0;
          this.documentResource.name = datas.searchData;
          this.dataSource.loadDocuments(this.documentResource);
        }
      }
    );
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.sub$.sink = merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap((c: any) => {
          this.documentResource.skip = this.paginator.pageIndex;
          this.documentResource.pageSize = this.paginator.pageSize;
          this.documentResource.orderBy = this.sort.active + ' ' + this.sort.direction;
          this.dataSource.loadDocuments(this.documentResource);
        })
      )
      .subscribe();

      if (this.input) {
        this.sub$.sink = fromEvent(this.input.nativeElement, 'keyup')
          .pipe(
            debounceTime(1000),
            distinctUntilChanged(),
            tap(() => {
              this.paginator.pageIndex = 0;
              this.documentResource.name = this.input.nativeElement.value;
              this.dataSource.loadDocuments(this.documentResource);
            })
          ).subscribe();
      }

      // this.sub$.sink = fromEvent(this.metatag.nativeElement, 'keyup')
      // .pipe(
      //   debounceTime(1000),
      //   distinctUntilChanged(),
      //   tap(() => {
      //     this.paginator.pageIndex = 0;
      //     this.documentResource.metaTags = this.metatag.nativeElement.value;
      //     this.dataSource.loadDocuments(this.documentResource);
      //   })
      // ).subscribe();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {

    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onAnalystChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentResource.metaTags = filtervalue.value;
    } else {
      this.documentResource.metaTags = '';
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onCategoryChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentResource.categoryId = filtervalue.value;
    } else {
      this.documentResource.categoryId = '';
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onSectorChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentResource.categoryId = filtervalue.value;

      if(filtervalue.value != this.currentCatId){
        this.categoryService.getSubCatOnlyOneLevelByParentId(filtervalue.value).subscribe(
          (c:any) => {
          this.listSubSectors = c.body;
          this.subSectorVal = "";
          this.companyVal = "";
          this.listCompanies = [];
        });
      }
    } else {
      this.documentResource.categoryId = '';
      this.subSectorVal = "";
      this.companyVal = "";
      this.listSubSectors = [];
      this.listCompanies = [];
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onSubSectorChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentResource.categoryId = filtervalue.value;

      this.categoryService.getSubCatOnlyOneLevelByParentId(filtervalue.value).subscribe(
        (c:any) => {
        this.listCompanies = c.body;
        this.companyVal = "";
      })
    } else {
      this.documentResource.categoryId = this.sectorVal;
      this.companyVal = "";
      this.listCompanies = [];
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onCompanyChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentResource.categoryId = filtervalue.value;
    } else {
      this.documentResource.categoryId = this.subSectorVal;
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onCreatedDateChange(filtervalue: any) {
    if (filtervalue) {
      this.documentResource.createDate = filtervalue;
    } else {
      this.documentResource.createDate = null;
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onPeriodDateSubmit() {
    if (this.rangeDatePicker.get("start").value && this.rangeDatePicker.get("end").value) {
      let temp_startDate:Date = this.rangeDatePicker.get("start").value;
      let start_mm = (temp_startDate.getMonth()+1).toString();
      let start_dd = temp_startDate.getDate().toString();

      if ((temp_startDate.getMonth()+1) < 10) start_mm = '0' + start_mm;
      if (temp_startDate.getDate() < 10) start_dd = '0' + start_dd;

      const startDate:string = temp_startDate.getFullYear() + "-" + start_mm + "-" + start_dd;

      let temp_endDate:Date = this.rangeDatePicker.get("end").value;
      let end_mm = (temp_endDate.getMonth()+1).toString();
      let end_dd = temp_endDate.getDate().toString();

      if ((temp_endDate.getMonth()+1) < 10) end_mm = '0' + end_mm;
      if (temp_endDate.getDate() < 10) end_dd = '0' + end_dd;

      const endDate:string = temp_endDate.getFullYear() + "-" + end_mm + "-" + end_dd;

      this.documentResource.startDate = startDate;
      this.documentResource.endDate = endDate;
    } else {
      this.documentResource.startDate = "";
      this.documentResource.endDate = "";
    }
    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
  }

  onPeriodDateReset() {
    this.documentResource.startDate = "";
    this.documentResource.endDate = "";

    this.documentResource.skip = 0;
    this.dataSource.loadDocuments(this.documentResource);
    this.rangeDatePicker.setValue({start: null, end: null});
  }

  getCategories() {
    this.categoryService.getAllCategoriesForDropDown().subscribe( c => {
      this.categories = [...c];
      // const currentCatReport = this.route.parent.snapshot.url[this.route.parent.snapshot.url.length - 1].path;
      this.categories.map((c) => {
        if (c.description == this.currentCatUrlActive) {
          localStorage.setItem('idCatReport', c.id);
          this.documentResource.categoryId = c.id;
          this.currentCatName = c.name != "" || c.name != null ? c.name : "Document";
          this.currentCatId = c.id != "" || c.id != null ? c.id : "";
        }
        // else if (c.description == currentCatReport) {
        //   localStorage.setItem('idCatReport', c.id);
        // } else if (c.description == currentCatReport) {
        //   localStorage.setItem('idCatReport', c.id);
        // }
      });
      this.tempCatId = [localStorage.getItem('idCatReport')];
      this.allCategories = [];
      this.getResourceParameter();

      this.documentResource.pageSize = 5;
      this.documentResource.skip = 0;
      this.documentResource.metaTags = "";
      this.documentResource.startDate = "";
      this.documentResource.name = "";
      this.paginator.pageIndex = 0;

      this.dataSource.loadDocuments(this.documentResource);

      // this.setDeafLevel();

      this.categoryService.getSubCatOnlyOneLevelByParentId(localStorage.getItem('idCatReport')).subscribe(
        (c:any) => {
        this.listSectors = c.body;
        this.listSubSectors = [];
        this.listCompanies = [];
      })
    });
  }

  setDeafLevel(parent?: Category, parentId?: string) {
    const children = this.categories.filter(c => c.parentId == parentId);
    if (children.length > 0) {
      children.map((c, index) => {
        c.deafLevel = parent ? parent.deafLevel + 1 : 0;
        c.index = (parent ? parent.index : 0) + index * Math.pow(0.1, c.deafLevel);
        if (localStorage.getItem('idCatReport')) {
          if (c.parentId != null) {
            this.tempCatId.push(c.id);
          }
          this.tempCatId.map((val, idx) =>{
            if (val == c.id || val == c.parentId) {
              this.allCategories.push(c);
              this.tempCatId.splice(idx, 1);
              this.setDeafLevel(c, c.id);
            }
            // else if (val == c.parentId) {
            //   this.allCategories.push(c);
            //   this.tempCatId.splice(idx, 1);
            //   this.setDeafLevel(c, c.id);
            // }
          })
        } else {
          this.allCategories.push(c);
          this.setDeafLevel(c, c.id);
        }
      });
    }
    return parent;
  }

  getResourceParameter() {
    this.sub$.sink = this.dataSource.responseHeaderSubject$
      .subscribe((c: ResponseHeader) => {
        if (c) {
          this.documentResource.pageSize = c.pageSize;
          this.documentResource.skip = c.skip;
          this.documentResource.totalCount = c.totalCount;
        }
      });
  }

  deleteDocument(document: DocumentInfo) {
    this.sub$.sink = this.commonDialogService
      .deleteConformationDialog(`${this.translationService.getValue('ARE_YOU_SURE_YOU_WANT_TO_DELETE')} ${document.name}`)
      .subscribe((isTrue: boolean) => {
        if (isTrue) {
          this.sub$.sink = this.documentService.deleteDocument(document.id)
            .subscribe(() => {
              this.addDocumentTrail(document.id, DocumentOperation.Deleted.toString());
              this.toastrService.success(this.translationService.getValue('DOCUMENT_DELETED_SUCCESSFULLY'));
              this.dataSource.loadDocuments(this.documentResource);
            });
        }
      });
  }

  getDocuments(): void {
    this.isLoadingResults = true;

    this.sub$.sink = this.documentService.getDocuments2(this.documentResource)
      .subscribe(
        (resp: HttpResponse<DocumentInfo[]>) => {
          const paginationParam = JSON.parse(
            resp.headers.get('X-Pagination')
          ) as ResponseHeader;
          this.documentResource.pageSize = paginationParam.pageSize;
          this.documentResource.skip = paginationParam.skip;
          this.documents = [...resp.body];
          this.isLoadingResults = false;
        },
        () => (this.isLoadingResults = false)
      );
  }

  editDocument(documentInfo: DocumentInfo) {
    const datas: any = {
      document: documentInfo,
      categories: this.categories,
      allAnalyst: this.allAnalyst,
      currentCatUrlActive: this.currentCatUrlActive
    }
    const dialogRef = this.dialog.open(DocumentEditComponent, {
      width: '600px',
      height: '80vh',
      data: Object.assign({}, datas)
    });

    this.sub$.sink = dialogRef.afterClosed()
      .subscribe((result: string) => {
        if (result === 'loaded') {
          this.dataSource.loadDocuments(this.documentResource);
        }
      });
  }

  manageDocumentPermission(documentInfo: DocumentInfo) {
    this.dialog.open(DocumentPermissionListComponent,
      {
        data: documentInfo,
        width: '80vw',
        height: '80vh'
      });
  }
  onSharedSelectDocument() {
    this.dialog.open(DocumentPermissionMultipleComponent,
      {
        data: this.selection.selected,
        width: '80vw',
        height: '80vh'
      });
  }

  uploadNewVersion(document: Document) {
    const dialogRef = this.dialog.open(DocumentUploadNewVersionComponent, {
      width: '800px',
      maxHeight: '70vh',
      data: Object.assign({}, document)
    });

    this.sub$.sink = dialogRef.afterClosed()
      .subscribe((result: string) => {
        if (result === 'loaded') {
          this.dataSource.loadDocuments(this.documentResource);
        }
      });

  }

  downloadDocument(documentInfo: DocumentInfo) {
    this.sub$.sink = this.commonService.downloadDocument(documentInfo.id, false).subscribe(
      (event) => {
        if (event.type === HttpEventType.Response) {
          this.addDocumentTrail(documentInfo.id, DocumentOperation.Download.toString());
          this.documentService.downloadFile(event, documentInfo);
          // this.downloadFile(event, documentInfo);
        }
      },
      (error) => {
        this.toastrService.error(this.translationService.getValue('ERROR_WHILE_DOWNLOADING_DOCUMENT'));
      }
    );
  }

  addDocumentTrail(id: string, operation: string) {
    const objDocumentAuditTrail: DocumentAuditTrail = {
      documentId: id,
      operationName: operation
    };
    this.sub$.sink = this.commonService.addDocumentAuditTrail(objDocumentAuditTrail)
      .subscribe(c => {
      });
  }

  onDocumentView(document: DocumentInfo) {
    const urls = document.url.split('.');
    const extension = urls[1];
    const documentView: DocumentView = {
      documentId: document.id,
      name: document.name,
      extension: extension,
      isRestricted: document.isAllowDownload,
      isVersion: false,
      isFromPreview: true
    };
    this.overlay.open(BasePreviewComponent, {
      position: 'center',
      origin: 'global',
      panelClass: ['file-preview-overlay-container', 'white-background'],
      data: documentView
    });
  }

  private async downloadFile(data: HttpResponse<Blob>, documentInfo: DocumentInfo) {
    let downloadedFile = new Blob([data.body], { type: data.body.type });
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    a.download = documentInfo.name;
    a.href = URL.createObjectURL(downloadedFile);
    a.target = '_blank';
    a.click();
    document.body.removeChild(a);
  }

  onVersionHistoryClick(document: DocumentInfo): void {
    let documentInfo = this.clonerService.deepClone<DocumentInfo>(document);
    this.sub$.sink = this.documentService
      .getDocumentVersion(document.id)
      .subscribe((documentVersions: DocumentVersion[]) => {
        documentInfo.documentVersions = documentVersions;
        this.dialog.open(DocumentVersionHistoryComponent, {
          width: '65vw',
          panelClass: 'full-width-dialog',
          data: Object.assign({}, documentInfo),
        });
      });
  }

  onGetLinkClick(document: DocumentInfo): void {
    let documentInfo = this.clonerService.deepClone<DocumentInfo>(document);
    this.sub$.sink = this.documentService
      .getDocumentVersion(document.id)
      .subscribe((documentVersions: DocumentVersion[]) => {
        documentInfo.documentVersions = documentVersions;
        this.dialog.open(DocumentGetLinkComponent, {
          width: '50vw',
          panelClass: 'full-width-dialog',
          data: Object.assign({}, documentInfo),
        });
      });
  }

  getAllAnalyst(){
    this.sub$.sink = this.analystService
    .getAllAnalyst()
    .subscribe((c) => {
      this.allAnalyst = c;
    });
  }
}
