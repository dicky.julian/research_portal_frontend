import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';

@Component({
  selector: 'app-document-get-link',
  templateUrl: './document-get-link.component.html',
  styleUrls: ['./document-get-link.component.css'],
  animations: [
    trigger('textCopied', [
      state('dimOn', style({
        backgroundColor: 'gray'
      })),
      state('dimOff', style({
        backgroundColor: 'white'
      })),
      transition('dimOn <=> dimOff', [
        animate('1s')
      ])
    ])
  ],
})
export class DocumentGetLinkComponent extends BaseComponent implements OnInit {
  docLink:string = "";
  isTextCopyAnimation = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DocumentInfo,
    private toastrService: ToastrService,
    private translationService: TranslationService
  ) {
    super()
   }

  ngOnInit(): void {
    let tempUrl = location.href.replace(location.pathname, '');
    this.docLink = tempUrl + "/doc-view?z=" + this.data.id;
  }

  onClickCopyToClipboard(){
    this.isTextCopyAnimation = true;
    try {
      navigator.clipboard.writeText(this.docLink);
      this.toastrService.success(this.translationService.getValue(`LINK_COPIED`));
      setTimeout(() => {
        this.isTextCopyAnimation = false;
      }, 500)
    } catch (error) {
      this.isTextCopyAnimation = false;
      this.toastrService.error(this.translationService.getValue(`LINK_COPIED_FAILED`));
    }
  }
}
