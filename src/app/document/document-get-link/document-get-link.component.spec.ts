import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentGetLinkComponent } from './document-get-link.component';

describe('DocumentGetLinkComponent', () => {
  let component: DocumentGetLinkComponent;
  let fixture: ComponentFixture<DocumentGetLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentGetLinkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DocumentGetLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
