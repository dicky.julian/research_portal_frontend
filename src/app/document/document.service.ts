import { HttpClient, HttpHeaderResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentResource } from '@core/domain-classes/document-resource';
import { DocumentVersion } from '@core/domain-classes/documentVersion';
import { CommonError } from '@core/error-handler/common-error';
import { CommonHttpErrorService } from '@core/error-handler/common-http-error.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PDFDocument, rgb } from 'pdf-lib';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@core/services/translation.service';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(
    private httpClient: HttpClient,
    private commonHttpErrorService: CommonHttpErrorService,
    private toastrService: ToastrService,
    private translationService: TranslationService
    ) {
  }

  updateDocument(document: DocumentInfo): Observable<DocumentInfo | CommonError> {
    const url = `document/${document.id}`;
    return this.httpClient.put<DocumentInfo>(url, document)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  addDocument(document: DocumentInfo): Observable<DocumentInfo | CommonError> {
    const url = `document`;
    return this.httpClient.post<DocumentInfo>(url, document)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  deleteDocument(id: string): Observable<void | CommonError> {
    const url = `document/${id}`;
    return this.httpClient.delete<void>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getDocument(id: string): Observable<DocumentInfo | CommonError> {
    const url = `document/${id}`;
    return this.httpClient.get<DocumentInfo>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getDocuments(resource: DocumentResource): Observable<HttpResponse<DocumentInfo[]> | CommonError> {
    const url = `documents`;
    const customParams = new HttpParams()
      .set('Fields', resource.fields)
      .set('OrderBy', resource.orderBy)
      .set('createDateString', resource.createDate ? resource.createDate.toString() : '')
      .set('PageSize', resource.pageSize.toString())
      .set('Skip', resource.skip.toString())
      .set('SearchQuery', resource.searchQuery)
      .set('categoryId', resource.categoryId)
      .set('name', resource.name)
      .set('metaTags', resource.metaTags)
      .set('id', resource.id.toString())

    return this.httpClient.get<DocumentInfo[]>(url, {
      params: customParams,
      observe: 'response'
    }).pipe(catchError(this.commonHttpErrorService.handleError));
  }

  saveNewVersionDocument(document): Observable<DocumentInfo | CommonError> {
    const url = `documentVersion`;
    return this.httpClient.post<DocumentInfo>(url, document)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getDocumentVersion(id: string) {
    const url = `documentversion/${id}`;
    return this.httpClient.get<DocumentVersion[]>(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  restoreDocumentVersion(id: string, versionId: string) {
    const url = `documentversion/${id}/restore/${versionId}`;
    return this.httpClient.post<boolean>(url, {})
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  getdocumentMetadataById(id: string) {
    const url = `document/${id}/getMetatag`;
    return this.httpClient.get(url)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  async getDocumentIPOType(id: string): Promise<boolean> {
    try {
      let params = new HttpParams()
        .set('id', id);

      const response = await this.httpClient.get<{ status: boolean }>(
        `__apiUrl2__/research/ipo-doc`, 
        { params: params }
      ).toPromise();

      return response?.status;
    } catch (error) {
      return false;
    }
  }

  async downloadFile(data: HttpResponse<Blob>, documentInfo: any) {
    let downloadedFile = new Blob([data.body], { type: data.body.type });
    let docId = documentInfo.documentId;

    this.getDocument(docId).subscribe(
      async (doc) => {
        if (data.body.type == 'application/pdf') {
          const isIPO = await this.getDocumentIPOType(docId);
          const pdfBytes = await this.addWaterMarkToPdf(downloadedFile, doc, isIPO);
          downloadedFile = new Blob([pdfBytes], { type: data.body.type });
        }

        const a = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        a.download = documentInfo.name;
        a.href = URL.createObjectURL(downloadedFile);
        a.target = '_blank';
        a.click();
        document.body.removeChild(a);
        this.toastrService.success(this.translationService.getValue('SUCCESS_DOWNLOADING_DOCUMENT'));
      }, (err) => {
        this.toastrService.error(this.translationService.getValue('ERROR_WHILE_DOWNLOADING_DOCUMENT'));
      }
    );
  }

  private async addWaterMarkToPdf(downloadedFile:Blob, documentInfo: any, isIPO?: boolean){
    const existingPdfBytes = await fetch(URL.createObjectURL(downloadedFile)).then(res => res.arrayBuffer());

    const pdfDoc = await PDFDocument.load(existingPdfBytes);

    const tempReq = new XMLHttpRequest();
    tempReq.open('GET', "", false);
    tempReq.send(null);
    const currentDate = new Date(tempReq.getResponseHeader("Date"));

    let currentUserName = 'Guest'
    if (JSON.parse(localStorage.getItem('authObj')).userName != 'guest@mail.com') {
      currentUserName = JSON.parse(localStorage.getItem('authObj')).userName;
    }

    const docDate = new Date(documentInfo.createdDate);

    const waterMarkText = environment.downloadedWatermark + " " + currentUserName;
    const dateTimeDownload = currentDate.getDate() + '/' + (currentDate.getMonth()+1) + '/' + currentDate.getFullYear() + ' - ' + currentDate.getHours() + ':' + currentDate.getMinutes() + ':' + currentDate.getSeconds();
    const copyRight = '© Copyright ' + docDate.getFullYear() + ' PT.BNI Sekuritas. All rights reserved';

    const pages = pdfDoc.getPages();

    if (isIPO) {
      const pngUrl = 'https://i.ibb.co/my10t2w/BNI-SEKURITAS.png';
      const pngImageBytes = await fetch(pngUrl).then((res) => res.arrayBuffer());
      const pngImage = await pdfDoc.embedPng(pngImageBytes);
      const pngDims = pngImage.scale(1);

      pages.forEach(async (page) => {
        page.drawImage(pngImage, {
          x: page.getWidth() / 2 - pngDims.width / 2,
          y: page.getHeight() / 2 - pngDims.height / 2,
          width: pngDims.height * 2 / 3,
          height: pngDims.height,
        })
      });
    }
    
    pages.forEach(async (page) => {
      let { width, height } = page.getSize();
      page.drawText(waterMarkText , {
        x: (width/2) - 35,
        y: height - 15,
        size: 9,
        // font: helveticaFont,
        color: rgb(253/255, 53/255, 53/255),
        // rotate: degrees(90),
      });

      page.drawText(dateTimeDownload , {
        x: (width/2) - 35,
        y: height - 25,
        size: 9,
        // font: helveticaFont,
        color: rgb(253/255, 53/255, 53/255),
        // rotate: degrees(90),
      });
    });

    return await pdfDoc.save();
  }

  // ENV USE URL API 2
  getDocuments2(resource: DocumentResource): Observable<HttpResponse<DocumentInfo[]> | CommonError> {
    const url = `__apiUrl2__/research/document`;
    const customParams = new HttpParams()
      .set('ParentId', resource.categoryId != ''? resource.categoryId : localStorage.getItem('idCatReport'))
      .set('RoleId', localStorage.getItem('userRole'))
      .set('UserId', JSON.parse(localStorage.getItem('authObj')).id)
      .set('Size', resource.pageSize.toString())
      .set('Page', (resource.skip + 1).toString())
      .set('Query', resource.name)
      .set('Sort', (resource.orderBy).split(' ')[1])
      .set('Analyst', resource.metaTags)
      .set('StartDate', resource.startDate)
      .set('EndDate', resource.endDate)

    return this.httpClient.get<DocumentInfo[]>(url, {
      params: customParams,
      observe: 'response'
    }).pipe(catchError(this.commonHttpErrorService.handleError));
  }

  updateDocumentDate(docId: string, docCreatedDate:string): Observable<DocumentInfo | CommonError> {
    const url = `__apiUrl2__/research/document`;
    const data = {
      id: docId,
      createdDate: docCreatedDate
    }
    return this.httpClient.put<DocumentInfo>(url, data)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }

  updateDocNewVersionDate(docId: string, date:string): Observable<DocumentInfo | CommonError> {
    const url = `__apiUrl2__/research/document/date`;
    const data = {
      id: docId,
      date: date
    }
    return this.httpClient.put<DocumentInfo>(url, data)
      .pipe(catchError(this.commonHttpErrorService.handleError));
  }
}
