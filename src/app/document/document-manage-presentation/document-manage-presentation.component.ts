import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators,FormArray, FormBuilder, FormGroup, } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from '@core/domain-classes/category';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentMetaData } from '@core/domain-classes/documentMetaData';
import { FileInfo } from '@core/domain-classes/file-info';
import { CategoryService } from '@core/services/category.service';
import { environment } from '@environments/environment';
import { BaseComponent } from 'src/app/base.component';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AnalystService } from 'src/app/analyst/analyst.service';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@core/services/translation.service';
import { values } from 'pdf-lib';

@Component({
  selector: 'app-document-manage-presentation',
  templateUrl: './document-manage-presentation.component.html',
  styleUrls: ['./document-manage-presentation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentManagePresentationComponent extends BaseComponent implements OnInit {
  document: DocumentInfo;
  documentForm: UntypedFormGroup;
  extension: string = '';
  categories: Category[] = [];
  allCategories: Category[] = [];
  allAnalyst: any[] = [];
  @Input() loading: boolean;
  documentSource: string;
  @Output() onSaveDocument: EventEmitter<DocumentInfo> = new EventEmitter<DocumentInfo>();
  progress: number = 0;
  message: string = '';
  fileInfo: FileInfo;
  showProgress: boolean = false;
  isFileUpload: boolean = false;
  tempCatId:string[] = [];
  isAnalystValid:boolean = true;

  currentCatName:string = "Document";
  currentCatId:string = "";
  currentCatUrlActive:string = "";

  sectorVal:string = "";
  subSectorVal:string = "";
  companyVal:string = "";
  selectedAnalyst:string ="";

  listSectors:Array<any> = new Array();
  listSubSectors:Array<any> = new Array();
  listCompanies:Array<any> = new Array();

  get documentMetaTagsArray(): FormArray {
    return <FormArray>this.documentForm.get('documentMetaTags');
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
        'fontName'
      ],
      [
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
        'backgroundColor'
      ]
    ]
  };

  constructor(
    private fb: UntypedFormBuilder,
    private httpClient: HttpClient,
    private cd: ChangeDetectorRef,
    private categoryService: CategoryService,
    public router: Router,
    private analystService: AnalystService,
    private toastService: ToastrService,
    private translationService:TranslationService,
    private route:ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {

    this.route.params.subscribe(
      params => {
        this.currentCatUrlActive = params.cat;

        this.sectorVal = "";
        this.subSectorVal = "";
        this.companyVal = "";
        this.selectedAnalyst = "";

        this.getCategories();
        this.createDocumentForm();
        this.documentMetaTagsArray.push(this.buildDocumentMetaTag());
        this.getAllAnalyst();

      }
    );

  }

  getCategories() {
    this.categoryService.getAllCategoriesForDropDown().subscribe(c => {
      this.categories = c;
      this.categories.map((c) => {
        if (c.description == this.currentCatUrlActive) {
          this.currentCatName = c.name != "" || c.name != null ? c.name : "Document";
          this.currentCatId = c.id != "" || c.id != null ? c.id : "";

          if (this.currentCatUrlActive == "macroeconomic-assumptions") {
            this.documentForm.get('categoryId').setValue(this.currentCatId);
            this.documentForm.updateValueAndValidity();
          }
        }
      });

      this.categoryService.getSubCatOnlyOneLevelByParentId(localStorage.getItem('idCatReport')).subscribe(
        (c:any) => {
          this.listSectors = c.body;
          this.listSubSectors = [];
          this.listCompanies = [];
      });

      this.tempCatId = [localStorage.getItem('idCatReport')];
      // this.setDeafLevel();
    });
  }

  setDeafLevel(parent?: Category, parentId?: string) {
    const children = this.categories.filter(c => c.parentId == parentId);
    if (children.length > 0) {
      children.map((c, index) => {
        c.deafLevel = parent ? parent.deafLevel + 1 : 0;
        c.index = (parent ? parent.index : 0) + index * Math.pow(0.1, c.deafLevel);
        if (localStorage.getItem('idCatReport')) {
          if (c.parentId != null) {
            this.tempCatId.push(c.id);
          }
          this.tempCatId.map((val, idx) =>{
            if (val == c.id || val == c.parentId) {
              this.allCategories.push(c);
              this.tempCatId.splice(idx, 1);
              this.setDeafLevel(c, c.id);
            }
            // else if () {
            //   this.allCategories.push(c);
            //   this.tempCatId.splice(idx, 1);
            //   this.setDeafLevel(c, c.id);
            // }
          })
        } else {
          this.allCategories.push(c);
          this.setDeafLevel(c, c.id);
        }
      });
    }
    return parent;
  }

  onDocumentChange($event: any) {
    const files = $event.target.files || $event.srcElement.files;
    const file_url = files[0];
    this.extension = file_url.name.split('.').pop();
    if (this.fileExtesionValidation(this.extension)) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        this.documentSource = e.target.result;
        this.fileUploadValidation('upload');
      }
      reader.readAsDataURL(file_url);
    } else {
      this.documentSource = null;
      this.fileUploadValidation('');
    }
  }

  fileUploadValidation(fileName: string) {
    this.documentForm.patchValue({
      url: fileName
    })
    this.documentForm.get('url').markAsTouched();
    this.documentForm.updateValueAndValidity();
  }

  fileUploadSizeValidation(fileSize: string) {
    this.documentForm.patchValue({
      fileSize: fileSize
    })
    this.documentForm.get('fileSize').markAsTouched();
    this.documentForm.updateValueAndValidity();
  }

  fileUploadExtensionValidation(extension: string) {
    this.documentForm.patchValue({
      extension: extension
    })
    this.documentForm.get('extension').markAsTouched();
    this.documentForm.updateValueAndValidity();
  }

  fileExtesionValidation(extesion: string): boolean {
    const allowExtesions = environment.allowExtesions;
    var allowTypeExtenstion = allowExtesions.find(c => c.extentions.find(ext => ext === extesion));
    return allowTypeExtenstion ? true : false;
  }

  createDocumentForm() {
    this.documentForm = this.fb.group({
      name: ['', [Validators.required]],
      description: [''],
      categoryId: ['', [Validators.required]],
      url: ['', [Validators.required]],
      fileSize: ['', [Validators.required]],
      extension: ['', [Validators.required]],
      documentMetaTags: this.fb.array([]),
      createdDate: ['', [Validators.required]],
    });
  }

  buildDocumentMetaTag(): FormGroup {
    return this.fb.group({
      id: [''],
      documentId: [''],
      metatag: ['']
    });
  }

  onMetatagChange(event: any, index: number) {
    const analystName = this.documentMetaTagsArray.at(index).get('metatag').value;
    if (!analystName) {
      return;
    }
    const analystNameControl = this.documentMetaTagsArray.at(index).get('metatag');
    analystNameControl.setValidators([Validators.required]);
    analystNameControl.updateValueAndValidity();
    this.isAnalystValid = true;

  }

  editDocmentMetaData(documentMetatag: DocumentMetaData): FormGroup {
    return this.fb.group({
      id: [documentMetatag.id],
      documentId: [documentMetatag.documentId],
      metatag: [documentMetatag.metatag]
    });
  }

  onSectorChange(filtervalue: any) {
    this.documentForm.get('categoryId').setValue(filtervalue.value);
    this.documentForm.updateValueAndValidity();

    if (filtervalue.value) {
      if (filtervalue.value != this.currentCatId) {
        this.categoryService.getSubCatOnlyOneLevelByParentId(filtervalue.value).subscribe(
          (c:any) => {
          this.listSubSectors = c.body;
          this.subSectorVal = "";
          this.companyVal = "";
          this.listCompanies = [];
        });
      }
    } else {
      this.subSectorVal = "";
      this.companyVal = "";
      this.listSubSectors = [];
      this.listCompanies = [];
    }
  }

  onSubSectorChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentForm.get('categoryId').setValue(filtervalue.value);
      this.documentForm.updateValueAndValidity();
      this.categoryService.getSubCatOnlyOneLevelByParentId(filtervalue.value).subscribe(
        (c:any) => {
        this.listCompanies = c.body;
        this.companyVal = "";
      });
    } else {
      this.documentForm.get('categoryId').setValue(this.sectorVal);
      this.documentForm.updateValueAndValidity();

      this.companyVal = "";
      this.listCompanies = [];
    }
  }

  onCompanyChange(filtervalue: any) {
    if (filtervalue.value) {
      this.documentForm.get('categoryId').setValue(filtervalue.value);
      this.documentForm.updateValueAndValidity();
    } else {
      this.documentForm.get('categoryId').setValue(this.subSectorVal);
      this.documentForm.updateValueAndValidity();
    }
  }

  SaveDocument() {
    this.isAnalystValid = true;
    this.documentMetaTagsArray.value.forEach((element) => {
      if (element.metatag == '') {
        this.isAnalystValid = false;
        return;
      } else{
        if (this.documentMetaTagsArray.value.filter(c => c.metatag == element.metatag).length > 1) {
          this.isAnalystValid = false;
          return;
        }
      }
    });

    if (this.documentForm.valid && this.isAnalystValid) {
      if(this.documentForm.get("name").value.trim() != ""){
        this.onSaveDocument.emit(this.buildDocumentObject());
      } else {
        this.toastService.error(this.translationService.getValue('NAME_IS_REQUIRED'))
      }

    } else {
      this.markFormGroupTouched(this.documentForm);
    }
  }

  private markFormGroupTouched(formGroup: UntypedFormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  buildDocumentObject(): DocumentInfo {
    const tempReq = new XMLHttpRequest();
    tempReq.open('GET', "", false);
    tempReq.send(null);
    const currentDate = new Date(tempReq.getResponseHeader("Date"));
    var createdDate = new Date(this.documentForm.get('createdDate').value);
    createdDate.setHours(currentDate.getHours());
    createdDate.setMinutes(currentDate.getMinutes());
    createdDate.setSeconds(currentDate.getSeconds());

    const documentMetaTags = this.documentMetaTagsArray.value;
    const document: DocumentInfo = {
      id: this.fileInfo.id,
      categoryId: this.documentForm.get('categoryId').value,
      description: (this.documentForm.get('description').value).trim(),
      name: (this.documentForm.get('name').value).trim(),
      url: this.fileInfo.fileName,
      documentMetaDatas: [...documentMetaTags],
      createdDate: createdDate
    };
    return document;
  }

  onAddAnotherMetaTag() {
    const documentMetaTag: DocumentMetaData = {
      id: '',
      documentId: this.document && this.document.id ? this.document.id : '',
      metatag: ''
    }
    this.documentMetaTagsArray.insert(0, this.editDocmentMetaData(documentMetaTag));
  }

  onDeleteMetaTag(index: number) {
    this.documentMetaTagsArray.removeAt(index);
  }

  upload(files) {
    if (files.length === 0)
      return;
    this.extension = files[0].name.split('.').pop();
    this.showProgress = true;
    if (!this.fileExtesionValidation(this.extension)) {
      this.fileUploadExtensionValidation('');
      this.showProgress = false;
      this.cd.markForCheck();
      return;
    } else {
      this.fileUploadExtensionValidation('valid');
    }
    const size = files[0].size;
    // if (size > environment.maximumFileSize) {
    //   this.fileUploadSizeValidation('');
    //   this.showProgress = false;
    //   this.cd.markForCheck();
    //   return;
    // } else {
    //   this.fileUploadSizeValidation('valid');
    // }
    this.fileUploadSizeValidation('valid')
    const formData = new FormData();
    for (let file of files)
      formData.append(file.name, file);
    const uploadReq = new HttpRequest('POST', `api/document/upload`, formData, {
      reportProgress: true,
    });

    this.sub$.sink = this.httpClient.request(uploadReq)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
          this.cd.markForCheck();
        }
        else if (event.type === HttpEventType.Response) {
          this.fileInfo = event.body as FileInfo;
          this.fileUploadValidation(this.fileInfo.fileName);
          this.isFileUpload = true;
          this.cd.markForCheck();
        }
      });
  }

  getAllAnalyst(){
    this.sub$.sink = this.analystService
    .getAllAnalyst()
    .subscribe((c) => {
      this.allAnalyst = c;
    });
  }
}

