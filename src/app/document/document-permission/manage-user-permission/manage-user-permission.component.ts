import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DocumentUserPermission } from '@core/domain-classes/document-user-permission';
import { User } from '@core/domain-classes/user';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { DocumentPermissionService } from '../document-permission.service';
import { take, takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';

@Component({
  selector: 'app-manage-user-permission',
  templateUrl: './manage-user-permission.component.html',
  styleUrls: ['./manage-user-permission.component.scss']
})
export class ManageUserPermissionComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  selectedUsers: User[] = [];
  minDate: Date;
  permissionForm: UntypedFormGroup;
  public userMultiFilterCtrl: FormControl<string> = new FormControl<string>('');
  public filteredUsersMulti: ReplaySubject<User[]> = new ReplaySubject<User[]>(1);
  protected _onDestroy = new Subject<void>();
  @ViewChild('multiSelect', { static: true }) multiSelect: MatSelect;
  public userMultiCtrl: FormControl<User[]> = new FormControl<User[]>([]);

  constructor(
    private documentPermissionService: DocumentPermissionService,
    private toastrService: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: { users: User[], documentId: string },
    private dialogRef: MatDialogRef<ManageUserPermissionComponent>,
    private fb: UntypedFormBuilder,
    private translationService:TranslationService) {
    super();
    this.minDate = new Date();
  }

  ngOnInit(): void {
    this.createUserPermissionForm();

    this.filteredUsersMulti.next(this.data.users);

    this.userMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUserMulti();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredUsersMulti
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.multiSelect.compareWith = (a: User, b: User) => a && b && a.id === b.id;
      });
  }

  protected filterUserMulti() {
    if (!this.data.users) {
      return;
    }

    let search = this.userMultiFilterCtrl.value;
    if (!search) {
      this.filteredUsersMulti.next(this.data.users);
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredUsersMulti.next(
      this.data.users.filter(user => user.firstName.toLowerCase().indexOf(search) > -1 || user.email.toLowerCase().indexOf(search) > -1)
    );
  }

  createUserPermissionForm() {
    this.permissionForm = this.fb.group({
      isTimeBound: new UntypedFormControl(false),
      startDate: [''],
      endDate: [''],
      isAllowDownload: new UntypedFormControl(false),
    });
  }

  timeBoundChange(event: MatCheckboxChange) {
    if (event.checked) {
      this.permissionForm.get('startDate').setValidators([Validators.required]);
      this.permissionForm.get('endDate').setValidators([Validators.required])
    } else {
      this.permissionForm.get('startDate').clearValidators();
      this.permissionForm.get('startDate').updateValueAndValidity();
      this.permissionForm.get('endDate').clearValidators();
      this.permissionForm.get('endDate').updateValueAndValidity();
    }
  }

  saveDocumentUserPermission() {
    if (!this.permissionForm.valid) {
      this.permissionForm.markAllAsTouched();
      return;
    }
    if (this.selectedUsers.length == 0) {
      this.toastrService.error(this.translationService.getValue('PLEASE_SELECT_ATLEAST_ONE_USER'));
      return
    }
    let documentUserPermission: DocumentUserPermission[] = this.selectedUsers.map(user => {
      return Object.assign({}, {
        id: '',
        documentId: this.data.documentId,
        userId: user.id
      }, this.permissionForm.value)
    });

    this.sub$.sink = this.documentPermissionService.addDocumentUserPermission(documentUserPermission).subscribe(() => {
      this.toastrService.success(this.translationService.getValue('PERMISSION_ADDED_SUCCESSFULLY'));
      this.dialogRef.close(true);
    });
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
