import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlDocViewComponent } from './url-doc-view.component';

describe('UrlDocViewComponent', () => {
  let component: UrlDocViewComponent;
  let fixture: ComponentFixture<UrlDocViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrlDocViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UrlDocViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
