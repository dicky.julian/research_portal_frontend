import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentView } from '@core/domain-classes/document-view';
import { OverlayPanel } from '@shared/overlay-panel/overlay-panel.service';
import { BasePreviewComponent } from '@shared/base-preview/base-preview.component';
import { DocumentService } from 'src/app/document/document.service';
import { DocumentInfo } from '@core/domain-classes/document-info';

@Component({
  selector: 'app-url-doc-view',
  templateUrl: './url-doc-view.component.html',
  styleUrls: ['./url-doc-view.component.css']
})
export class UrlDocViewComponent extends BaseComponent implements OnInit {

  constructor (
    private route: ActivatedRoute,
    public overlay: OverlayPanel,
    private documentService: DocumentService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({
      next: (params: any) => {
        if (params['z'] && params['z'] != null) {
          this.onDocumentView(params['z'].toString());
        }
      },
    });
  }

  onDocumentView(id:string) {
    this.sub$.sink = this.documentService.getDocument(id).subscribe(
      (data: DocumentInfo) => {
        const urls = data.url.split('.');
        const extension = urls[1];
        const documentView: DocumentView = {
          documentId: data.id,
          name: data.name,
          extension: extension,
          isRestricted: data.isAllowDownload,
          isVersion: false,
          isFromPreview: false
        };
        this.overlay.open(BasePreviewComponent, {
          position: 'center',
          origin: 'global',
          panelClass: ['file-preview-overlay-container', 'white-background'],
          data: documentView
        });
      },
      (error) => {
        this.router.navigate(['/home']);
      }
    )
  }
}
