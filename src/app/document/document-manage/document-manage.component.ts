import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { BaseComponent } from 'src/app/base.component';
import { DocumentService } from '../document.service';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { Router } from '@angular/router';
import { DocumentAuditTrail } from '@core/domain-classes/document-audit-trail';
import { DocumentOperation } from '@core/domain-classes/document-operation';
import { CommonService } from '@core/services/common.service';
import { TranslationService } from '@core/services/translation.service';
import { DocumentPermissionService } from '../document-permission/document-permission.service';
import { Role } from '@core/domain-classes/role';
import { DocumentRolePermission } from '@core/domain-classes/document-role-permission';

@Component({
  selector: 'app-document-manage',
  templateUrl: './document-manage.component.html',
  styleUrls: ['./document-manage.component.scss']
})
export class DocumentManageComponent extends BaseComponent implements OnInit {
  documentForm: UntypedFormGroup;
  loading$: Observable<boolean>;
  documentSource: string;

  constructor(
    private toastrService: ToastrService,
    private documentService: DocumentService,
    private router: Router,
    private commonService: CommonService,
    private translationService: TranslationService,
    private documentPermissionService: DocumentPermissionService,
    ) {
    super();
  }

  ngOnInit(): void {
  }

  saveDocument(document: DocumentInfo) {
    this.sub$.sink = this.documentService.addDocument(document)
      .subscribe((documentInfo: DocumentInfo) => {

        let mm = (document.createdDate.getMonth()+1).toString();
        let dd = document.createdDate.getDate().toString();
        let hh = document.createdDate.getHours().toString();
        let minutes = document.createdDate.getMinutes().toString();
        let ss = document.createdDate.getSeconds().toString();

        if ((document.createdDate.getMonth()+1) < 10) mm = '0' + mm;
        if (document.createdDate.getDate() < 10) dd = '0' + dd;
        if (document.createdDate.getHours() < 10) hh = '0' + hh;
        if (document.createdDate.getMinutes() < 10) minutes = '0' + minutes;
        if (document.createdDate.getSeconds() < 10) ss = '0' + ss;

        const createdDate = document.createdDate.getFullYear() + "-" + mm + "-" + dd  + 'T' + hh + ':' + minutes + ':' + ss + "Z";

        this.documentService.updateDocumentDate(documentInfo.id, createdDate).subscribe(
          d => {
            this.sub$.sink = this.commonService.getRoles().subscribe((roles: Role[]) => {
              let documentRolePermission: DocumentRolePermission[] = [];
              roles.forEach(role => {
                if (role.id == '02748681-fd6d-456e-be38-312916f2912c' || role.id == '2bfc619d-6e13-48ac-869e-3daa59c65115') {
                  documentRolePermission.push(
                    Object.assign({}, {
                      id: '',
                      documentId: documentInfo.id,
                      roleId: role.id,
                      startDate: null,
                      endDate: null,
                      isAllowDownload: true
                    })
                  );
                } else if (role.id == '3ad91c85-f648-4abd-adac-426779ccf06b') {
                  documentRolePermission.push(
                    Object.assign({}, {
                      id: '',
                      documentId: documentInfo.id,
                      roleId: role.id,
                      startDate: null,
                      endDate: null,
                      isAllowDownload: false
                    })
                  );
                }

                // temporary auto assign user
                if( role.id == "fa04647a-0a36-4472-94ae-3826f2421237"){
                  documentRolePermission.push(
                    Object.assign({}, {
                      id: '',
                      documentId: documentInfo.id,
                      roleId: role.id,
                      startDate: null,
                      endDate: null,
                      isAllowDownload: true
                    })
                  );
                }
              })

              // let documentRolePermission: DocumentRolePermission[] = roles.map(role => {
              //   return Object.assign({}, {
              //     id: '',
              //     documentId: documentInfo.id,
              //     roleId: role.id,
              //     startDate: null,
              //     endDate: null,
              //     isAllowDownload: true
              //   })
              // });

              this.documentPermissionService.addDocumentRolePermission(documentRolePermission).subscribe(() => {
                this.addDocumentTrail(documentInfo.id);
                this.toastrService.success(this.translationService.getValue('DOCUMENT_SAVE_SUCCESSFULLY'));
                this.router.navigate([this.router.url.replace('/add', '')]);
              });
            });
          }
        );
      });
  }
  addDocumentTrail(id: string) {
    const objDocumentAuditTrail: DocumentAuditTrail = {
      documentId: id,
      operationName: DocumentOperation.Created.toString()
    }
    this.sub$.sink = this.commonService.addDocumentAuditTrail(objDocumentAuditTrail)
      .subscribe(c => {
      })
  }

}
