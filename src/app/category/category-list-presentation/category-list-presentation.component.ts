import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output }
  from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CommonDialogService } from '@core/common-dialog/common-dialog.service';
import { Category } from '@core/domain-classes/category';
import { CategoryService } from '@core/services/category.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { ManageCategoryComponent } from '../manage-category/manage-category.component';

@Component({
  selector: 'app-category-list-presentation',
  templateUrl: './category-list-presentation.component.html',
  styleUrls: ['./category-list-presentation.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('10ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ],
})
export class CategoryListPresentationComponent extends BaseComponent implements OnInit {

  @Input() categories: Category[];
  @Output() addEditCategoryHandler: EventEmitter<Category> = new EventEmitter<Category>();
  @Output() deleteCategoryHandler: EventEmitter<string> = new EventEmitter<string>();
  columnsToDisplay: string[] = ['subcategory', 'action', 'name'];
  subCategoryColumnToDisplay: string[] = ['subcategory', 'action', 'name'];
  subCategoryColumnToDisplay2: string[] = ['subcategory', 'action', 'name'];
  subCategoryColumnToDisplay3: string[] = ['action', 'name'];
  subCategories1: Category[] = [];
  subCategories2: Category[] = [];
  subCategories3: Category[] = [];
  expandedElement1: Category | null;
  expandedElement2: Category | null;
  expandedElement3: Category | null;
  constructor(
    private dialog: MatDialog,
    private commonDialogService: CommonDialogService,
    private cd: ChangeDetectorRef,
    private categoryService: CategoryService,
    private translationService:TranslationService,
    private toastrService:ToastrService
  ) {
    super();
  }

  ngOnInit(): void {
  }

  toggleRow(element: Category) {

    this.categoryService.getSubCategories(element.id).subscribe(subCat => {
      this.subCategories1 = [];
      this.subCategories1 = subCat;
      // this.expandedElement1 = this.expandedElement1 === element ? null : element;
      if (this.expandedElement1 && (this.expandedElement1.id === element.id)) {
        this.expandedElement1 = null;
        this.expandedElement2 = null;
        this.expandedElement3 = null;
      } else {
        this.expandedElement1 = element;
        this.expandedElement2 = null;
        this.expandedElement3 = null;
      }
      this.cd.detectChanges();
    });
  }

  toggleRow2(element: Category) {

    this.categoryService.getSubCategories(element.id).subscribe(subCat => {
      this.subCategories2 = [];
      this.subCategories2 = subCat;
      // this.expandedElement2 = this.expandedElement2 === element ? null : element;
      if (this.expandedElement2 && (this.expandedElement2.id === element.id)) {
        this.expandedElement2 = null;
        this.expandedElement3 = null;
      } else {
        this.expandedElement2 = element;
        this.expandedElement3 = null;
      }
      this.cd.detectChanges();
    });
  }

  toggleRow3(element: Category) {

    this.categoryService.getSubCategories(element.id).subscribe(subCat => {
      this.subCategories3 = [];
      this.subCategories3 = subCat;
      // this.expandedElement3 = this.expandedElement3 === element ? null : element;
      if (this.expandedElement3 && (this.expandedElement3.id === element.id)) {
        this.expandedElement3 = null;
      } else {
        this.expandedElement3 = element;
      }
      this.cd.detectChanges();
    });
  }

  deleteCategory(category: Category): void {
    this.sub$.sink = this.commonDialogService
      .deleteConformationDialog(`${this.translationService.getValue('ARE_YOU_SURE_YOU_WANT_TO_DELETE')} ${category.name}`)
      .subscribe(isTrue => {
        if (isTrue) {
          // this.deleteCategoryHandler.emit(category.id);
          this.categoryService.delete(category.id).subscribe(d => {
            this.toastrService.success(this.translationService.getValue(`CATEGORY_DELETED_SUCCESSFULLY`));

            if (this.expandedElement1) {
              this.categoryService.getSubCategories(this.expandedElement1.id).subscribe(subCat1 => {
                this.subCategories1 = [];
                this.subCategories1 = subCat1;
                this.cd.detectChanges();

                if (this.expandedElement2) {
                  this.categoryService.getSubCategories(this.expandedElement2.id).subscribe(subCat2 => {
                    this.subCategories2 = [];
                    this.subCategories2 = subCat2;
                    this.cd.detectChanges();

                    if (this.expandedElement3) {
                      this.categoryService.getSubCategories(this.expandedElement3.id).subscribe(subCat3 => {
                        this.subCategories3 = [];
                        this.subCategories3 = subCat3;
                        this.cd.detectChanges();
                      });
                    }
                  });
                }
              });
            }

          });

        }
      });
  }

  manageCategory(category: Category): void {
    const dialogRef = this.dialog.open(ManageCategoryComponent, {
      width: '350px',
      data: Object.assign({}, category)
    });

    this.sub$.sink = dialogRef.afterClosed()
      .subscribe((result: Category) => {
        if (result) {
          // this.addEditCategoryHandler.emit(result);
          if (this.expandedElement1) {
            this.categoryService.getSubCategories(this.expandedElement1.id).subscribe(subCat1 => {
              this.subCategories1 = [];
              this.subCategories1 = subCat1;
              this.cd.detectChanges();

              if (this.expandedElement2) {
                this.categoryService.getSubCategories(this.expandedElement2.id).subscribe(subCat2 => {
                  this.subCategories2 = [];
                  this.subCategories2 = subCat2;
                  this.cd.detectChanges();

                  if (this.expandedElement3) {
                    this.categoryService.getSubCategories(this.expandedElement3.id).subscribe(subCat3 => {
                      this.subCategories3 = [];
                      this.subCategories3 = subCat3;
                      this.cd.detectChanges();
                    });
                  }
                });
              }

            });
          }
        }
      });
  }

  addSubCategory(category: Category) {
    this.manageCategory({
      id: '',
      description: '',
      name: '',
      parentId: category.id
    });
  }
}
