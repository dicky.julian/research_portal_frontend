import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/security/auth.guard';
import { LayoutComponent } from './core/layout/layout.component';
import { MyProfileComponent } from './user/my-profile/my-profile.component';
import { ViewDocByUrlComponent } from './view-doc-by-url/view-doc-by-url.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module')
        .then(m => m.LoginModule)
  },
  {
    path: 'doc-view',
    component: ViewDocByUrlComponent,
  }, {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        loadChildren: () =>
          import('./dashboard/dashboard.module')
            .then(m => m.DashboardModule)
      }, {
        path: 'my-profile',
        component: MyProfileComponent,
        canActivate: [AuthGuard],
      }, {
        path: 'home',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./dashboard/dashboard.module')
            .then(m => m.DashboardModule)
      },
      {
        path: 'operations',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./operation/operation.module')
            .then(m => m.OperationModule)
      },
      {
        path: 'screens',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./screen/screen.module')
            .then(m => m.ScreenModule)
      }, {
        path: 'screen-operation',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./screen-operation/screen-operation.module')
            .then(m => m.ScreenOperationModule)
      },
      {
        path: 'roles',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./role/role.module')
            .then(m => m.RoleModule)
      },
      {
        path: 'users',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./user/user.module')
            .then(m => m.UserModule)
      },
      {
        path: 'categories',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./category/category.module')
            .then(m => m.CategoryModule)
      },
      {
        path: 'documents',
        canActivateChild: [AuthGuard],
        loadChildren: () =>
          import('./document/document.module')
            .then(m => m.DocumentModule)
      },
      {
        path: 'document/:cat',
        canActivateChild: [AuthGuard],
        loadChildren: () =>
          import('./document/document.module')
            .then(m => m.DocumentModule)
      },
      {
        path: 'document-audit-trails',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./document-audit-trail/document-audit-trail.module')
            .then(m => m.DocumentAuditTrailModule)
      }, {
        path: 'login-audit',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./login-audit/login-audit.module')
            .then(m => m.LoginAuditModule)
      },
      {
        path: 'analysts',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./analyst/analyst.module')
            .then(m => m.AnalystModule)
      },
      {
        path: '**',
        redirectTo: '/'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
