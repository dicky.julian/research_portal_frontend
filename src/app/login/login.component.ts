import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '../base.component';
import { Router, ActivatedRoute  } from '@angular/router';
import { UserAuth } from '@core/domain-classes/user-auth';
import { SecurityService } from '@core/security/security.service';
import { ToastrService } from 'ngx-toastr';
import { CommonError } from '@core/error-handler/common-error';
import { MatDialog } from '@angular/material/dialog';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { UserService } from '../user/user.service';
import { User } from '@core/domain-classes/user';
import { RoleService } from '../role/role.service';
import packageJson from 'package.json';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {

  version: string = packageJson.version;

  @ViewChild('password') passwordInput: ElementRef<HTMLInputElement>;
  isPassShowed = false;

  loginFormGroup: UntypedFormGroup;
  isLoading = false;
  lat: number;
  lng: number;

  returnUrl: string;

  constructor(
    private fb: UntypedFormBuilder,
    private router: Router,
    private securityService: SecurityService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private userService: UserService,
    private roleService: RoleService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.createFormGroup();
    navigator.geolocation.getCurrentPosition((position) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';
  }

  onLoginSubmit() {
    if (this.loginFormGroup.valid) {
      this.isLoading = true;
      var userObject = Object.assign(this.loginFormGroup.value, { latitude: this.lat, longitude: this.lng });
      this.sub$.sink = this.securityService.login(userObject)
        .subscribe(
          (userAuthResp: UserAuth) => {

            this.userService.getUser(userAuthResp.id).subscribe(
              (userResp:User) => {
                localStorage.setItem('userRole', userResp.userRoles[0].roleId);

                this.roleService.getRole(userResp.userRoles[0].roleId).subscribe(
                  (roleResp:any) => {
                    this.isLoading = false;

                    if (roleResp.id == '3ad91c85-f648-4abd-adac-426779ccf06b') {
                      localStorage.setItem('isGuest', 'true');
                      if (!(location.pathname == '/doc-view')) {
                        this.securityService.logout();
                        this.toastr.error('Role guest do not have login permition');
                      }
                    } else {
                      this.securityService.checkResetPassword().subscribe(
                        (d:any) => {

                          if (this.loginFormGroup.get("rememberMe").value) {
                            localStorage.setItem("isRememberMe", "true");
                          } else {
                            localStorage.setItem("isRememberMe", "false");
                          }

                          this.toastr.success('User login successfully.');

                          if (d.body.isReset) {
                            if(localStorage.getItem('isGuest') != 'true'){
                              localStorage.setItem('isResetPassword', 'true');
                              this.router.navigate(['/my-profile']);
                            }
                          } else {
                            this.router.navigateByUrl(this.returnUrl);
                          }
                        },
                        (err: CommonError) => {
                          this.isLoading = false;
                          err.messages.forEach(msg => {
                            this.toastr.error(msg);
                            this.securityService.logout();
                          });
                        }
                      )
                    }
                  },
                  (err: CommonError) => {
                    this.isLoading = false;
                    err.messages.forEach(msg => {
                      this.toastr.error(msg);
                      this.securityService.logout();
                    });
                  }
                )
              },
              (err: CommonError) => {
                this.isLoading = false;
                err.messages.forEach(msg => {
                  this.toastr.error(msg);
                  this.securityService.logout();
                });
              }
            )

            // if (this.securityService.hasClaim('dashboard_view_dashboard')) {
            //   this.router.navigate(['/dashboard']);
            // }
            // else {
            //   this.router.navigate(['/']);
            // }
          },
          (err: CommonError) => {

            this.isLoading = false;
            err.messages.forEach(msg => {
              this.toastr.error(msg)
            });
          }
        );
    }
  }

  createFormGroup(): void {
    this.loginFormGroup = this.fb.group({
      userName: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      rememberMe: ['true']
    });
  }

  showHidePass() {
    if (this.passwordInput.nativeElement.type === "password") {
      this.passwordInput.nativeElement.type = "text";
      this.isPassShowed = true;
    } else {
      this.passwordInput.nativeElement.type = "password";
      this.isPassShowed = false;
    }
  }

  onRegistrationClick(): void {
    this.router.navigate(['/registration']);
  }

  onForgetPassClick(){
    this.dialog.open(ForgetPasswordComponent, {
      width: '30vw',
      panelClass: ['full-width-dialog', 'dialog-forget-pass-container'],
    });
  }

}
