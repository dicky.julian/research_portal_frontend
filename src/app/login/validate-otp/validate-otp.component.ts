import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SecurityService } from '@core/security/security.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { NgOtpInputComponent } from 'ng-otp-input';

@Component({
  selector: 'app-validate-otp',
  templateUrl: './validate-otp.component.html',
  styleUrls: ['./validate-otp.component.css']
})
export class ValidateOtpComponent extends BaseComponent implements OnInit {

  otpFormGroup: UntypedFormGroup;
  otp: string;
  isOtpSended:boolean = false;
  @ViewChild(NgOtpInputComponent, { static: false}) ngOtpInput:NgOtpInputComponent;

  timer:number = 300;
  isCanResend:boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ValidateOtpComponent>,
    private fb: UntypedFormBuilder,
    private toastrService: ToastrService,
    private translationService: TranslationService,
    private securityService: SecurityService,
    @Inject(MAT_DIALOG_DATA) public data: any,

  ) { super(); }

  ngOnInit(): void {
    this.createFormGroup();
  }

  ngAfterViewInit() {
    setInterval(() => {
      if (this.timer > 0) {
        this.timer--;
      } else {
        this.isCanResend = true;
      }
    }, 1000);
  }

  onOtpChange(otp) {
    this.otpFormGroup.get('otp').setValue(otp);
  }

  createFormGroup(): void {
    this.otpFormGroup = this.fb.group({
      otp: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  onSendClick(){
    if (this.otpFormGroup.valid) {
      const datas = {
        otp: this.otpFormGroup.get('otp').value,
        requestid: this.data.requestid
      }
      this.securityService.validateOTP(datas)
        .subscribe((c) => {
          if(c.status){
            this.toastrService.success(this.translationService.getValue(`OTP_SEND_SUCCESSFULLY`));
            this.isOtpSended = true;
          } else{
            this.toastrService.error(this.translationService.getValue(`OTP_SEND_UNSUCCESSFULLY`));
          }
        },
        (e) => {
          this.toastrService.error(this.translationService.getValue(`OTP_SEND_UNSUCCESSFULLY`));
        });
    }
  }
  onCloseClick(): void {
    this.dialogRef.close();
  }

  onResendClick(){
    if (this.data.email) {
      const datas = {
        email: this.data.email
      }
      this.securityService.sendOTP(datas)
      .subscribe((c) => {
        this.toastrService.success(this.translationService.getValue(`EMAIL_SENT_SUCCESSFULLY`));
        this.data.requestid = c.requestid;
        this.ngOtpInput.setValue('');
        this.timer = 300;
        this.isCanResend = false;
      }, (e) => {
        this.toastrService.error(this.translationService.getValue('EMAIL_SENT_UNSUCCESSFULLY'));
      });
    }
  }
}
