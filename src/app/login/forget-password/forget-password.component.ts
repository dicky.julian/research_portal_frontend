import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SecurityService } from '@core/security/security.service';
import { TranslationService } from '@core/services/translation.service';
import { ToastrService } from 'ngx-toastr';
import { ValidateOtpComponent } from '../validate-otp/validate-otp.component';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  forgetPassFormGroup: UntypedFormGroup;

  constructor(
    public dialogRef: MatDialogRef<ForgetPasswordComponent>,
    private fb: UntypedFormBuilder,
    private toastrService: ToastrService,
    private translationService: TranslationService,
    private dialog: MatDialog,
    private securityService: SecurityService
  ) { }

  ngOnInit(): void {
    this.createFormGroup();
  }
  createFormGroup(): void {
    this.forgetPassFormGroup = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  onSendClick(){
    if (this.forgetPassFormGroup.valid) {
      const datas = {
        email: this.forgetPassFormGroup.get('email').value
      }
      this.securityService.sendOTP(datas)
      .subscribe((c) => {
        this.toastrService.success(this.translationService.getValue(`EMAIL_SENT_SUCCESSFULLY`));
        this.dialogRef.close();
        this.dialog.open(ValidateOtpComponent, {
          width: '35vw',
          panelClass:  ['full-width-dialog', 'dialog-forget-pass-container'],
          disableClose: true,
          data: Object.assign({}, c, datas),
        });
      }, (e) => {
        this.toastrService.error(this.translationService.getValue('EMAIL_SENT_UNSUCCESSFULLY'));
      });
    }

  }
  onCloseClick(): void {
    this.dialogRef.close();
  }

}
