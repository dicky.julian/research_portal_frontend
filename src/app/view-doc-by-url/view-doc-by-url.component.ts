import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentView } from '@core/domain-classes/document-view';
import { CommonError } from '@core/error-handler/common-error';
import { SecurityService } from '@core/security/security.service';
import { CommonService } from '@core/services/common.service';
import { TranslationService } from '@core/services/translation.service';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from '../base.component';
import { DocumentService } from '../document/document.service';
import { RoleService } from '../role/role.service';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-view-doc-by-url',
  templateUrl: './view-doc-by-url.component.html',
  styleUrls: ['./view-doc-by-url.component.scss']
})
export class ViewDocByUrlComponent extends BaseComponent implements OnInit, OnDestroy {

  docId = '';
  type = '';
  currentDoc: DocumentView;
  docDatas: DocumentInfo;
  isLoading: boolean = false;
  isGuest:boolean = true;
  // loadingTime: number = 2000;
  documentUrl: any = null;
  httpResponseData: HttpResponse<Blob>;
  lat: number;
  lng: number;

  constructor(
    private commonService: CommonService,
    private toastrService: ToastrService,
    private translationService: TranslationService,
    private router: Router,
    private route: ActivatedRoute,
    private documentService:DocumentService,
    private securityService: SecurityService,
    private userService: UserService,
    private roleService: RoleService
    ) {
    super();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.isGuest = true;

    this.onLoadDocDatas();

    // navigator.geolocation.getCurrentPosition((position) => {
    //   this.lat = position.coords.latitude;
    //   this.lng = position.coords.longitude;
    // });
  }

  @HostListener('window:beforeunload')
  ngOnDestroy(): void {
    if (localStorage.getItem('isGuest') == 'true'){
      this.securityService.logout();
    }
  }

  onLoadDocDatas() {
    this.route.queryParams.subscribe({
      next: (params: any) => {
        const documentId = params['z'];
        if (!documentId) return;

        this.docId = documentId;
        const isLoggedIn = localStorage.getItem('authObj');
        if (isLoggedIn) {
          this.getDocDatas(documentId);
          return;
        }

        this.sub$.sink = this.securityService
            .getDocumentAccessForGuest(documentId)
            .subscribe(
              () => {
                this.getDocDatas(documentId);
                localStorage.setItem('isGuest', 'true');
              },
              (err: CommonError) => {
                this.isLoading = false;
                this.securityService.logout();
                this.toastrService.error('Document uploaded more than 24 hours, login first!');
                this.router.navigateByUrl('/documents/doc-view?z=' + this.docId);
              }
            );
      },
    });
  }

  getDocDatas(documentId: string) {
    this.sub$.sink = this.documentService.getDocument(documentId).subscribe(
      (data: DocumentInfo) => {
        this.docDatas = data;
        
        const urls = data.url.split('.');
        const extension = urls[1];
        this.currentDoc = {
          documentId: data.id,
          name: data.name,
          extension: extension,
          isRestricted: data.isAllowDownload,
          isVersion: false,
          isFromPreview: false
        };

        this.getDocument();
        // this.checkDocDateAvailable();
      },
      (error) => {
        this.isLoading = false;
        this.securityService.logout();
        this.router.navigateByUrl('/');
      }
    )
  }

  onDocumentView() {
    const allowExtesions = environment.allowExtesions;
    const allowTypeExtenstion = allowExtesions.find(c => c.extentions.find(ext => ext === this.currentDoc.extension));
    this.type = allowTypeExtenstion ? allowTypeExtenstion.type : '';

    if (localStorage.getItem('isGuest') == 'true') {
      this.isGuest = true;
    } else {
      this.isGuest = false;
    }
  }

  downloadDocument() {
    this.documentService.downloadFile(this.httpResponseData, this.currentDoc);
    // this.sub$.sink = this.commonService.downloadDocument(this.currentDoc.documentId, documentView.isVersion).subscribe(
    //   (event) => {
    //     if (event.type === HttpEventType.Response) {
    //       this.documentService.downloadFile(event, documentView);
    //     }
    //   },
    //   (error) => {
    //     this.toastrService.error(this.translationService.getValue('ERROR_WHILE_DOWNLOADING_DOCUMENT'));
    //   }
    // );
  }

  getDocument() {
    this.sub$.sink = this.commonService.downloadDocument(this.currentDoc.documentId, this.currentDoc.isVersion)
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          this.isLoading = false;
          this.httpResponseData = event;
          this.downloadFile();
        }
      }, (err) => {
        this.isLoading = false;
        this.securityService.logout();
        this.router.navigateByUrl('/');
      });
  }

  downloadFile() {
    this.documentUrl = new Blob([this.httpResponseData.body], { type: this.httpResponseData.body.type });
    this.onDocumentView();
  }

  checkDocDateAvailable() {
    const then = new Date(this.docDatas.createdDate);
    const now = new Date();

    const msBetweenDates:any = Math.abs(then.getTime() - now.getTime());

    // convert ms to hours                  min  sec   ms
    const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);

    if (hoursBetweenDates < 24) {
      console.log('Doc is within 24 hours');
      this.getDocument();
    } else {
      console.log('Doc is NOT within 24 hours');
      this.isLoading = false;
      if(localStorage.getItem('authObj')){
        if (localStorage.getItem('isGuest') == 'true') {
          this.securityService.logout();
          this.toastrService.error('Document uploaded more than 24 hours, login first!');
          this.router.navigateByUrl('/documents/doc-view?z=' + this.docId);
        } else {
          this.getDocument();
        }
      }
    }
  }

}
