import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDocByUrlComponent } from './view-doc-by-url.component';

describe('ViewDocByUrlComponent', () => {
  let component: ViewDocByUrlComponent;
  let fixture: ComponentFixture<ViewDocByUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewDocByUrlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewDocByUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
