import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Category } from '@core/domain-classes/category';
import { SecurityService } from '@core/security/security.service';
import { CategoryService } from '@core/services/category.service';
import { BaseComponent } from 'src/app/base.component';
import { ContactInfoComponent } from 'src/app/contact-info/contact-info.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent extends BaseComponent implements OnInit {
  categories: Category[] = [];

  constructor(
    public router: Router,
    private categoryService: CategoryService,
    public securityService: SecurityService,
    private dialog:MatDialog
    ) {
      super()
     }

  ngOnInit(): void {
    this.getCategories();
  }
  addClass(event): void {
    event.target.className += 'showMenu';
  }

  removeClass(event): void {
    event.target.className = event.target.className.replace('removeMenu', '');
  }
  onActiveUrl() {
    if (this.router.url === '/operations' || this.router.url === '/screens' || this.router.url === '/screen-operation') {
      return "active";
    }
  }

  getCategories(): void {
    this.categoryService.getAllCategories().subscribe(c => {
      this.categories = [...c];
    });;
  }

  openContactInfo(){
    const dialogRef = this.dialog.open(ContactInfoComponent, {
      width: '450px',
      panelClass: 'full-width-dialog',
    });
  }

}
