import { UserClaim } from "./user-claim";
import { UserRoles } from "./user-roles";

export interface User {
  id?: string;
  userName: string;
  email: string;
  firstName?: string;
  lastName?: string;
  companyName?: string;
  password?: string;
  phoneNumber?: string;
  userRoles?: UserRoles[];
  clientCode?: string;
  idMgr?: string;
  ctyCode?: string;
  userClaims?: UserClaim[];
}

export interface UserList {
  accManager?: string;
  clientType?: string;
  companyName?: string;
  email?: string;
  id?: string;
  name?: string;
  phoneNumber?: string;
  userName?: string;
}