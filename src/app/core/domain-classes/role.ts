import { RoleClaim } from "./role-claim";
import { UserRoles } from "./user-roles";

export interface Role {
  id?: string;
  name?: string;
  userRoles?: UserRoles[];
  roleClaims?: RoleClaim[];
}

export interface ClientType {
  clientCode: string;
  clientType: string;
}

export interface AccManager {
  id_mgr: string;
  name: string;
}

export interface Country {
  ctyCode: string;
  countryName: string;
}