export interface Analyst {
  id?:string,
  name?:string,
  title?:string,
  email?:string,
  phone?:string,
  coverage?:string,
  bytePhoto?:string,
  photo?:string
}
