import { Component, OnInit, AfterViewChecked, DoCheck, HostListener } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewChecked, DoCheck {

  sideNavMode: 'side' | 'over' | 'push' = 'side';
  leftPositionDrawerToggle:string = '148px';

  vh = window.innerHeight * 0.01;

  constructor() {
  }

  ngOnInit(): void {
    this.vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${this.vh}px`);

    if (screen.width > 580 ) {
      this.sideNavMode = 'side';
    } else {
      this.sideNavMode = 'over'
    }
  }

  ngAfterViewChecked(): void {
    this.vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${this.vh}px`);

    if (screen.width > 580 ) {
      this.sideNavMode = 'side';
    } else {
      this.sideNavMode = 'over'
    }
  }

  ngDoCheck(): void {
    this.vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${this.vh}px`);

    if (screen.width > 580 ) {
      this.sideNavMode = 'side';
    } else {
      this.sideNavMode = 'over'
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${this.vh}px`);

    if (screen.width > 580 ) {
      this.sideNavMode = 'side';
    } else {
      this.sideNavMode = 'over'
    }
  }
}
