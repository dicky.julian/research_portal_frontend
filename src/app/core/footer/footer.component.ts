import { Component, OnInit } from '@angular/core';
import { environment } from '@environments/environment';
import packageJson from 'package.json';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  footerCC:string = environment.footerCC;
  versionApp: string = packageJson.version;

  constructor() { }

  ngOnInit(): void {
  }

}
