import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageFlag } from '@core/domain-classes/language-flag';
import { UserNotification } from '@core/domain-classes/notification';
import { UserAuth } from '@core/domain-classes/user-auth';
import { SecurityService } from '@core/security/security.service';
import { TranslationService } from '@core/services/translation.service';
import { BaseComponent } from 'src/app/base.component';
import { DataSharedService } from '@shared/data-shared/data-shared.service';
import { CommonDialogService } from '@core/common-dialog/common-dialog.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseComponent implements OnInit {
  @ViewChild('selectElem', { static: true }) el: ElementRef;
  @Input()
  public lead: any;
  navbarOpen = false;
  appUserAuth: UserAuth = null;
  newNotificationCount = 0;
  notifications: UserNotification[] = [];
  language: LanguageFlag;
  languages: LanguageFlag[] = [
    {
      lang: 'id',
      name: 'Indonesia',
      flag: '../../../assets/images/flags/indonesia.svg',
    },
    {
      lang: 'en',
      name: 'English',
      flag: '../../../assets/images/flags/united-states.svg',
    },

    // {
    //   lang: 'es',
    //   name: 'Spanish ',
    //   flag: '../../../assets/images/flags/brazil.svg',
    // },
    // {
    //   lang: 'fr',
    //   name: 'French ',
    //   flag: '../../../assets/images/flags/france.svg',
    // },
    // {
    //   lang: 'ar',
    //   name: 'Arabic ',
    //   flag: '../../../assets/images/flags/saudi-arabia.svg',
    // },
    // {
    //   lang: 'ru',
    //   name: 'Russian',
    //   flag: '../../../assets/images/flags/russia.svg',
    // },
    // {
    //   lang: 'ja',
    //   name: 'Japanese',
    //   flag: '../../../assets/images/flags/japan.svg',
    // },
    // {
    //   lang: 'ko',
    //   name: 'Korean',
    //   flag: '../../../assets/images/flags/south-korea.svg',
    // },
    // {
    //   lang: 'cn',
    //   name: 'Chinese',
    //   flag: '../../../assets/images/flags/china.svg',
    // }
  ];

  isSearch:boolean = false;
  @ViewChild('input') input: ElementRef;

  constructor(
    private router: Router,
    private securityService: SecurityService,
    private translationService: TranslationService,
    private sharedDataService: DataSharedService,
    private commonDialogService: CommonDialogService
    ) {
    super();
  }

  ngOnInit(): void {
    this.setTopLogAndName();
    // this.subscribeToNotification();
    this.setDefaultLanguage();

    this.sharedDataService.datas.subscribe(
      (datas) => {
        this.isSearch = datas.isSearchActive;
        if (this.input) {
          this.input.nativeElement.value = datas.searchData;
        }
      }
    );
  }

  setDefaultLanguage() {
    const lang = this.translationService.getSelectedLanguage();
    if (lang)
      this.setLanguageWithRefresh(lang);
  }


  setLanguageWithRefresh(lang: string) {
    this.languages.forEach((language: LanguageFlag) => {
      if (language.lang === lang) {
        language.active = true;
        this.language = language;
      } else {
        language.active = false;
      }
    });
    this.translationService.setLanguage(lang);
  }

  setNewLanguageRefresh(lang: string) {
    this.sub$.sink = this.translationService.setLanguage(lang).subscribe((response) => {
      this.setLanguageWithRefresh(response['LANGUAGE']);
    });
  }

  setTopLogAndName() {
    this.sub$.sink = this.securityService.SecurityObject.subscribe(c => {
      if (c) {
        this.appUserAuth = c;
      }
    })
  }

  public togglediv(): void {
    if (this.lead.className === 'toggled') {
      this.lead.className = '';
    } else {
      this.lead.className = 'toggled';
    }
  }

  onLogout(): void {
    this.sub$.sink = this.commonDialogService
      .deleteConformationDialog(`${this.translationService.getValue('LOGOUT_CONFIRMATION')}`)
      .subscribe((isTrue: boolean) => {
        if (isTrue) {
          this.securityService.logout();
          this.setLanguageWithRefresh('en');
          this.router.navigate(['/login']);
        }
      });
  }

  onMyProfile(): void {
    this.router.navigate(['/my-profile']);
  }

  onSearchSubmit(){
    // if (this.input.nativeElement.value.length >= 3) {
      this.sharedDataService.set_searchData((this.input.nativeElement.value).trim());
    // }
  }
}
