import { Component, HostListener, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OnlineUser } from '@core/domain-classes/online-user';
import { UserAuth } from '@core/domain-classes/user-auth';
import { SecurityService } from '@core/security/security.service';
import { SignalrService } from '@core/services/signalr.service';
import { TranslationService } from '@core/services/translation.service';
import { TranslateService } from '@ngx-translate/core';
import { DataSharedService } from '@shared/data-shared/data-shared.service';
import { BaseComponent } from './base.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewChecked {
  title = 'document-management';
  /**
   *
   */
  constructor(
    private signalrService: SignalrService,
    private securityService: SecurityService,
    public translate: TranslateService,
    private translationService: TranslationService,
    private router: Router,
    private sharedDataService: DataSharedService,
  ) {
    super();
    translate.addLangs(['en']);
    translate.setDefaultLang('en');
    this.setLanguage();
  }

  setLanguage() {
    const currentLang = this.translationService.getSelectedLanguage();
    if (currentLang) {
      this.sub$.sink = this.translationService.setLanguage(currentLang)
      .subscribe(() => { });
    }
    else {
      const browserLang = this.translate.getBrowserLang();
      // const lang = browserLang.match(/en|es|ar|ru|cn|ja|ko|fr/) ? browserLang : 'en';
      const lang = browserLang.match(/en|id/) ? browserLang : 'id';
      this.sub$.sink = this.translationService.setLanguage(lang).subscribe(() => { });
    }
  }

  ngOnInit(): void {
    // this.signalrService.startConnection().then(resolve => {
    //   if (resolve) {
    //     this.signalrService.handleMessage();
    //     this.getAuthObj();
    //   }
    // });
  }

  ngAfterViewChecked():void {
    if (
      this.router.url == "/document/company-report" ||
      this.router.url == "/document/sector-report" ||
      this.router.url == "/document/strategy-report" ||
      this.router.url == "/document/macroeconomic-report" ||
      this.router.url == "/document/macroeconomic-assumptions" ||
      this.router.url == "/document/fixed-income-research"
    ) {
      this.sharedDataService.set_isSearchActive(true);
    } else {
      this.sharedDataService.set_isSearchActive(false);
    }
  }

  @HostListener('window:beforeunload')
  ngOnDestroy(): void {
    // localStorage.removeItem('idCatReport');
    if (localStorage.getItem("isRememberMe") == "false") {
      this.securityService.logout();
    }

  }

  getAuthObj() {
    this.sub$.sink = this.securityService.SecurityObject
      .subscribe((c: UserAuth) => {
        if (c) {
          const online: OnlineUser = {
            email: c.email,
            id: c.id,
            connectionId: this.signalrService.connectionId
          };
          this.signalrService.addUser(online);
        }
      });
  }
}
