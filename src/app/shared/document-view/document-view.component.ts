import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { DocumentAuditTrail } from '@core/domain-classes/document-audit-trail';
import { DocumentInfo } from '@core/domain-classes/document-info';
import { DocumentOperation } from '@core/domain-classes/document-operation';
import { DocumentView } from '@core/domain-classes/document-view';
import { CommonService } from '@core/services/common.service';
import { OVERLAY_PANEL_DATA } from '@shared/overlay-panel/overlay-panel-data';
import { OverlayPanelRef } from '@shared/overlay-panel/overlay-panel-ref';
import { ToastrService } from 'ngx-toastr';
import { BaseComponent } from 'src/app/base.component';
import { DocumentService } from 'src/app/document/document.service';
import { TranslationService } from '@core/services/translation.service';

@Component({
  selector: 'app-document-view',
  templateUrl: './document-view.component.html',
  styleUrls: ['./document-view.component.css']
})
export class DocumentViewComponent extends BaseComponent implements OnInit {
  constructor(
    @Inject(OVERLAY_PANEL_DATA) public data: DocumentView,
    private overlayRef: OverlayPanelRef,
    private toastrService: ToastrService,
    private commonService: CommonService,
    private documentService: DocumentService,
    private translationService: TranslationService
    ) {
    super();
  }
  documentUrl: string = null;
  documentInfo: DocumentInfo;
  viewerType: any = 'pdf';
  isLoading: boolean = false;
  loadingTime: number = 2000;
  public get isPdf(): boolean {
    return this.viewerType === 'pdf';
  }

  ngOnInit(): void {

  }

  closeToolbar() {
    this.overlayRef.close();
  }

  addDocumentTrail() {
    const objDocumentAuditTrail: DocumentAuditTrail = {
      documentId: this.documentInfo.id,
      operationName: DocumentOperation.Read.toString()
    }
    this.sub$.sink = this.commonService.addDocumentAuditTrail(objDocumentAuditTrail)
      .subscribe(c => {
      })
  }

  getDocumentUrl() {
    this.documentUrl = this.documentInfo.documentSource;
    this.loadingTime = 0;
  }

  downloadDocument() {
    this.sub$.sink = this.commonService.downloadDocument(this.documentInfo.id, this.data.isVersion).subscribe(
      (event) => {
        if (event.type === HttpEventType.Response) {
          this.documentService.downloadFile(event, this.documentInfo);
          // this.downloadFile(event);
        }
      },
      (error) => {
        this.toastrService.error(this.translationService.getValue('ERROR_WHILE_DOWNLOADING_DOCUMENT'));
      }
    );
  }

  private downloadFile(data: HttpResponse<Blob>) {
    const downloadedFile = new Blob([data.body], { type: data.body.type });
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    a.download = this.documentInfo.name;
    a.href = URL.createObjectURL(downloadedFile);
    a.target = '_blank';
    a.click();
    document.body.removeChild(a);
  }
}
