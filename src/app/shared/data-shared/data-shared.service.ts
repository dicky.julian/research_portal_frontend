import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataShared } from './data-shared';

@Injectable({
  providedIn: 'root'
})
export class DataSharedService {

  private dataShared = new BehaviorSubject<DataShared>(
    {
      isSearchActive:false,
      searchData:'',
    }
  );

  datas = this.dataShared.asObservable();

  constructor() {
  }

  updateDatas(datas) {
    this.dataShared.next(datas);
  }

  set_isSearchActive(val:boolean){
    this.dataShared.getValue().isSearchActive = val;
    this.dataShared.next(this.dataShared.value);
  }

  set_searchData(val:string){
    this.dataShared.getValue().searchData = val;
    this.dataShared.next(this.dataShared.value);
  }
}
