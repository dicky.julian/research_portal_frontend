export const environment = {
  production: true,

  // PROD
  // apiUrl: 'https://research.bnisekuritas.co.id/',
  // apiUrl2: 'https://ws.bions.id/',

  // DEV
  apiUrl: 'https://research-dev.bnisekuritas.co.id/',
  apiUrl2: 'https://dev.bions.id:8000/',

  allowExtesions: [
    // {
    //   type: 'office',
    //   extentions: ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx']
    // },
    {
      type: 'pdf',
      extentions: ['pdf']
    },
    // {
    //   type: 'image',
    //   extentions: [
    //     'jpg',
    //     'jpeg',
    //     'png',
    //     'gif',
    //     'tiff',
    //     'psd',
    //     'bmp',
    //     'webp',
    //     'raw',
    //     'bmp',
    //     'heif',
    //     'indd',
    //     'svg',
    //     'ai',
    //     'eps'
    //   ]
    // },
    // {
    //   type: 'text',
    //   extentions: ['txt', 'csv']
    // },
    // {
    //   type: 'audio',
    //   extentions: [
    //     '3gp',
    //     'aa',
    //     'aac',
    //     'aax',
    //     'act',
    //     'aiff',
    //     'alac',
    //     'amr',
    //     'ape',
    //     'au',
    //     'awb',
    //     'dss',
    //     'dvf',
    //     'flac',
    //     'gsm',
    //     'iklx',
    //     'ivs',
    //     'm4a',
    //     'm4b',
    //     'm4p',
    //     'mmf',
    //     'mp3',
    //     'mpc',
    //     'msv',
    //     'nmf',
    //     'ogg',
    //     'oga',
    //     'mogg',
    //     'opus',
    //     'org',
    //     'ra',
    //     'rm',
    //     'raw',
    //     'rf64',
    //     'sln',
    //     'tta',
    //     'voc',
    //     'vox',
    //     'wav',
    //     'wma',
    //     'wv'
    //   ],
    // },
    // {
    //   type: 'video',
    //   extentions: [
    //     'webm',
    //     'flv',
    //     'vob',
    //     'ogv',
    //     'ogg',
    //     'drc',
    //     'avi',
    //     'mts',
    //     'm2ts',
    //     'wmv',
    //     'yuv',
    //     'viv',
    //     'mp4',
    //     'm4p',
    //     '3pg',
    //     'flv',
    //     'f4v',
    //     'f4a',

    //   ]
    // }
  ],
  allowPhotoExtesions: [
    {
      type: 'image',
      extentions: [
        'jpg',
        'jpeg',
        // 'png',
      ]
    },
  ],
  maximumFileSize: 104857600,
  prefixGeneratedPass: "akun@",
  suffixGeneratedPass: "2023",
  downloadedWatermark: "This report is prepared for",
  symbol: "~ ` ! @ # $ % ^ & * ( ) _ - + = { [ } ] | \\ : ; \" \' < , > . ? /",
  footerCC: "© Copyright 2023 PT.BNI Sekuritas. All rights reserved",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
